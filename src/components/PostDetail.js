import React from "react";
import { Dimensions, ScrollView, Linking } from "react-native";
import { View, Image, Title, Caption, Divider } from "@shoutem/ui";
import HTML from "react-native-render-html";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import moment from "moment";
import config from "../config";
import Entities from "html-entities";
import { Actions } from "react-native-router-flux";
import firebase from "react-native-firebase";

const entities = new Entities.AllHtmlEntities();
const window = Dimensions.get("window");

const styles = {
  featuredImageWrap: {
    aspectRatio: 1.78
  },
  featuredImage: {
    width: window.width,
    height: 256
  },
  html: {
    h1: {
      marginHorizontal: 16,
      marginTop: 8,
      marginBottom: 8
    },
    p: {
      marginHorizontal: 16,
      marginTop: 8,
      marginBottom: 8
    }
  }
};

export default class PostDetail extends React.Component {
  componentDidMount() {
    //Actions.refresh({title: entities.decode(this.props.item.title.rendered)});
    firebase
      .analytics()
      .logEvent("view_berita", { title: entities.decode(this.props.item.title.rendered) });
  }

  renderFeaturedImage = item => {
    if (item.featured_media === 0) return null;

    let image = item.featured_media_url;
    const featuredMedia = item._embedded["wp:featuredmedia"][0];
    if (!image && featuredMedia.media_details.sizes.medium) {
      image = featuredMedia.media_details.sizes.medium.source_url;
    }
    if (!image) image = featuredMedia.source_url;
    if (!image) {
      image = featuredMedia.media_details.sizes.full.source_url;
    }

    if (!image) return null;

    return <Image style={styles.featuredImage} source={{ uri: image }} />;
  };

  alterNode = node => {
    let { name, attribs } = node;
    //change http -> https , also remove image attribs
    if (name && name === "img") {
      let src = node.attribs.src.replace("http://", "https://");
      node.attribs = [];
      node.attribs.src = src;
      //console.log(node);
      return node;
    }
  };

  onLinkPress = (evt, href) => {
    //console.log(href);
    Linking.openURL(href);
  };

  render() {
    const { item, isBerita } = this.props;

    let newTitle = entities.decode(item.title.rendered);
    let content = (
      <View styleName="md-gutter">
        <Title>{newTitle}</Title>
        <Caption styleName="sm-gutter-top">{moment(item.date).format("LLL")}</Caption>
        <Divider styleName="sm-gutter-vertical line" />
        <HTML
          htmlStyles={styles.html}
          html={item.content.rendered}
          alterNode={this.alterNode}
          onLinkPress={this.onLinkPress}
          imagesMaxWidth={window.width - 32}
        />
      </View>
    );

    if (isBerita) {
      let imageToRender = this.renderFeaturedImage(item);

      if (imageToRender)
        return (
          <ParallaxScrollView
            parallaxHeaderHeight={window.width * (9 / 16)}
            renderBackground={() => imageToRender}
          >
            {content}
          </ParallaxScrollView>
        );
      else return <ScrollView styleName="fill-parent">{content}</ScrollView>;
    } else {
      return <ScrollView styleName="fill-parent">{content}</ScrollView>;
    }
  }
}
