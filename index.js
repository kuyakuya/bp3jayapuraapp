import { AppRegistry } from "react-native";
import App from "./App";
import { useStrict } from "mobx";

//remove console log if not on Simulator
if (!__DEV__) {
  console.log = () => {};
}

useStrict(true); // mobx - don't allow state modifications outside actions

AppRegistry.registerComponent("BP3JayapuraApp", () => App);
