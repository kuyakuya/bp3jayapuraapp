import config from "../config";
import axios from "axios";
import { cacheAdapterEnhancer } from "axios-extensions";

const WP_API_URL = config.API_URL + "/wp/v2";
const JWT_API_URL = config.API_URL + "/jwt-auth/v1";

const axiosWithCache = axios.create({
  baseURL: "/",
  headers: { "Cache-Control": "no-cache" },
  // cache will be enabled by default
  adapter: cacheAdapterEnhancer(axios.defaults.adapter, true)
});

const withQuery = (url, params = {}) => {
  const esc = encodeURIComponent;
  let query = Object.keys(params)
    .filter(key => params[key] !== "" && params[key] !== null)
    .map(key => `${esc(key)}=${esc(params[key])}`)
    .join("&");
  query = query.length > 0 ? `?${query}` : null;

  return `${url}${query}`;
};

export const getPosts = (params = {}) => {
  const defParams = { _embed: 1, per_page: 10, page: 1 };
  const url = withQuery(`${WP_API_URL}/posts`, { ...defParams, ...params });
  console.log(url);

  return axiosWithCache.get(url);
};

export const getPhotos = (params = {}) => {
  const defParams = { per_page: 12, page: 1 };
  const url = withQuery(`${WP_API_URL}/media`, { ...defParams, ...params });
  console.log(url);

  return axiosWithCache.get(url);
};

export const getJwtToken = (username, password) => {
  //get JWT token - make sure the JWT Auth plugin is configured on server!!!
  const url = `${JWT_API_URL}/token`;
  console.log(url);
  /*
	axios.interceptors.request.use(request => {
		console.log('Starting Request', request)
		return request
	})
	
	axios.interceptors.response.use(response => {
		console.log('Response:', response)
		return response
	})	
	*/

  return axios.post(url, {
    username: username,
    password: password
  });
};

export const getPage = id => {
  const url = `${WP_API_URL}/pages/${id}`;
  console.log(url);

  return axiosWithCache.get(url);
};
