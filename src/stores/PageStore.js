import { observable, action } from "mobx";
import { getPosts, getPage } from "../services/wordpressApi";
import { Alert } from "react-native";

export default class PageStore {
  @observable loading = observable.map();
  //@observable content = observable.map();
  content = new Map();

  async fetchPage(id) {
    this.setLoading(id, true);
    try {
      const response = await getPage(id);
      //console.log(response.data);
      this.updateContent(id, response.data);
    } catch (e) {
      Alert.alert("Error", e.message);
    } finally {
      this.setLoading(id, false);
    }
  }

  @action
  setLoading(id, loading) {
    //console.log("setLoading "+ loading);
    this.loading.set(id, loading);
  }

  updateContent(id, data) {
    this.content.set(id, data);
  }

  getContent(id) {
    //console.log("getItemsArray - "+ key);
    return this.content.has(id) ? this.content.get(id) : {};
  }
}
