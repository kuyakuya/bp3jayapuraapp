import React, { Component } from "react";
import { Screen, View, Text } from "@shoutem/ui";
import PostDetail from "../components/PostDetail";
import NavHeader from "../components/NavHeader";

export default class BeritaDetailScreen extends Component {
  render() {
    const { item, isBerita } = this.props.navigation.state.params;

    return (
      <Screen styleName="paper">
        <PostDetail item={item} isBerita={isBerita} />
      </Screen>
    );
  }
}
