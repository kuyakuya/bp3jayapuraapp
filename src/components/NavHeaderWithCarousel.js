import React, { Component } from "react";
import PropTypes from "prop-types";
import { Dimensions } from "react-native";
import { View, Overlay, Image, Title, Text } from "@shoutem/ui";
import ButtonAvatar from "./ButtonAvatar";
import { TouchableOpacity } from "@shoutem/ui/components/TouchableOpacity";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Actions } from "react-native-router-flux";
import { colors } from "../theme";
import { observer } from "mobx-react/native";
import { userStore } from "../stores";
import Carousel from "react-native-looped-carousel";
import ScaledImageAsset from "../components/ScaledImageAsset";

const window = Dimensions.get("window");
const navHeight = window.height * 0.45;

const Banner = ({ image }) => (
  <View style={{ width: window.width, height: "100%" }}>
    <ScaledImageAsset source={image} height={navHeight} />
  </View>
);

@observer
export default class NavHeaderWithCarousel extends Component {
  showDrawer = () => {
    Actions.drawerOpen();
  };

  showLogin = () => {
    Actions.push("login");
  };

  showProfile = () => {
    Actions.push("profile");
  };

  renderAvatar = () => {
    if (!userStore.isLoggedIn()) return <ButtonAvatar auth={false} onPress={this.showLogin} />;
    else {
      const profile = userStore.currentProfile;
      return (
        <ButtonAvatar
          auth={true}
          avatar={profile && profile.avatar ? profile.avatar : null}
          fullname={profile && profile.name_depan ? profile.nama_depan : null}
          onPress={this.showProfile}
        />
      );
    }
  };

  render() {
    return (
      <View style={{ height: navHeight }}>
        <Carousel
          delay={5000}
          style={{ width: window.width, height: "100%", backgroundColor: colors.green }}
          arrows={false}
          pageInfo={false}
        >
          <Banner image={require("../images/banner-01.jpg")} />
          <Banner image={require("../images/banner-02.jpg")} />
          <Banner image={require("../images/banner-03.jpg")} />
          <Banner image={require("../images/banner-04.jpg")} />
          <Banner image={require("../images/banner-05.jpg")} />
          <Banner image={require("../images/banner-06.jpg")} />
          {/*
          <Banner image={{ uri: "banner-1" }} />
          <Banner image={{ uri: "banner-2" }} />
          <Banner image={{ uri: "banner-3" }} />
          <Banner image={{ uri: "banner-4" }} />
          <Banner image={{ uri: "banner-5" }} />
          <Banner image={{ uri: "banner-6" }} />
        */}
        </Carousel>
        <Overlay styleName="fill-parent clear">
          <View styleName="fill-parent vertical v-start">
            <Image
              style={{ width: window.width, height: 160 }}
              resizeMode="stretch"
              source={require("../images/header.png")}
            />
          </View>
          {/* hamburger icon */}
          <View style={{ position: "absolute", top: 20, left: 10, padding: 5 }}>
            <TouchableOpacity onPress={this.showDrawer}>
              <Icon name="menu" size={36} color={colors.white} />
            </TouchableOpacity>
          </View>
          {/* avatar */}
          <View style={{ position: "absolute", top: 20, right: 20 }}>{this.renderAvatar()}</View>
        </Overlay>
      </View>
    );
  }
}

NavHeaderWithCarousel.propTypes = {
  compact: PropTypes.bool,
  title: PropTypes.string,
  imageUri: PropTypes.number
};
