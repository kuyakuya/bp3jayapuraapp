import { observable, action } from "mobx";
import { getPhotos } from "../services/wordpressApi";
import { getVideos } from "../services/youtubeApi";
import { Alert } from "react-native";

const MAX_PHOTO_PER_PAGE = 18;

export default class GalleryStore {
  @observable loading = observable.map();
  @observable page = observable.map();
  @observable items = observable.map();
  @observable moreData = observable.map();

  async fetchData(key, params = {}) {
    if (!this.page.get(key)) {
      this.setPage(key, 1);
    }

    if (this.page.get(key) === 1) this.setMoreData(key, true);

    this.setLoading(key, true);
    try {
      if (key === "photos") {
        params.per_page = MAX_PHOTO_PER_PAGE;
        const response = await getPhotos({ page: this.page.get(key), ...params });
        this.updateItems(key, response.data);
        //console.log(response.data.length);
        if (response.data.length < MAX_PHOTO_PER_PAGE) this.setMoreData(key, false);
      } else if (key === "videos") {
        const response = await getVideos({ page: this.page.get(key), ...params });
        let filtered = response.data.items.filter(item => item.id.kind === "youtube#video");
        this.updateItems(key, filtered);

        if (!response.data.nextPageToken) this.setMoreData(key, false);
      } else {
        Alert.alert("Unknown Key!");
      }
    } catch (e) {
      Alert.alert("Koneksi Error", e.message);
    } finally {
      this.setLoading(key, false);
    }
  }

  getItemsArray(key) {
    //console.log("getItemsArray - "+ key);
    return this.items.has(key) ? this.items.get(key).peek() : [];
  }

  @action
  setLoading(key, loading) {
    //console.log("setLoading "+ loading);
    this.loading.set(key, loading);
  }

  @action
  setMoreData(key, flag) {
    this.moreData.set(key, flag);
  }

  @action
  setPage(key, page) {
    this.page.set(key, page);
  }

  @action
  nextPage(key) {
    this.page.set(key, this.page.get(key) + 1);
  }

  @action
  updateItems(key, items) {
    //console.log("updateItems - "+ key);
    const add = this.page.get(key) > 1;
    if (this.items.has(key)) {
      this.items.set(key, add ? [...this.items.get(key), ...items] : items);
    } else {
      this.items.set(key, items);
    }
  }

  @action
  deleteItems(key) {
    if (this.items.has(key)) {
      //console.log("deleteItems - "+ key);
      this.items.delete(key);
    }
  }
}
