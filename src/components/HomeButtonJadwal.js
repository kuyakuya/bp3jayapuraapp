import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Text, Image, Title } from "@shoutem/ui";
import { colors } from "../theme";

export default class HomeButtonJadwal extends Component {
  render() {
    const { flex, onPress } = this.props;

    return (
      <TouchableOpacity
        style={{ flex: flex, backgroundColor: colors.yellow, margin: 1 }}
        onPress={onPress}
      >
        <View styleName="fill-parent horizontal h-end">
          <Image
            source={require("../images/dashboard-calendar.png")}
            style={{ position: "absolute", width: 120, height: 120, right: 0 }}
          />
        </View>
        <View styleName="fill-parent vertical md-gutter">
          <Title style={{ lineHeight: 20, color: colors.blue }} styleName="bold">
            Jadwal Akademik BP3 Jayapura
          </Title>
          <Text style={{ fontSize: 11, lineHeight: 12, marginTop: 5 }}>
            Informasi Jadwal Kegiatan Akademik di BP3 Jayapura untuk Tahun 2018
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

HomeButtonJadwal.propTypes = {
  flex: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired
};
