import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { View, Text, TextInput } from "@shoutem/ui";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Actions } from "react-native-router-flux";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import { colors } from "../theme";

export default class TextInputWithIcon extends Component {
  render() {
    const { icon, multiline, transparent } = this.props;
    const _icon = icon ? (
      <Icon name={icon} color={colors.lightGray} size={24} style={{ marginHorizontal: 8 }} />
    ) : null;
    let _inputStyle = { flex: 1 };
    if (multiline) _inputStyle = Object.assign(_inputStyle, { height: 128 });
    if (transparent)
      _inputStyle = Object.assign(_inputStyle, {
        backgroundColor: "transparent",
        color: colors.white,
        placeholderTextColor: colors.gray
      });

    return (
      <View
        styleName="horizontal v-center"
        style={{
          padding: 0,
          borderBottomWidth: 1,
          borderColor: colors.lightGray,
          marginBottom: 12
        }}
      >
        {_icon}
        <TextInput {...this.props} autoCorrect={false} style={_inputStyle} />
      </View>
    );
  }
}
