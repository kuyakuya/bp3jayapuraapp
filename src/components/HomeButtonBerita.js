import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Row, TouchableOpacity, Text, Title, Overlay, Image, Caption } from "@shoutem/ui";
import { ActivityIndicator } from "react-native";
import { postStore } from "../stores";
import { observer } from "mobx-react/native";
import config from "../config";
import Entities from "html-entities";
import { colors } from "../theme";
import ScaledImageAsset from "../components/ScaledImageAsset";

const entities = new Entities.AllHtmlEntities();

@observer
export default class HomeButtonBerita extends Component {
  componentWillMount() {
    this.key = "berita";
    this.qs = { categories_exclude: config.PENGUMUMAN_CATEGORY_ID };
  }

  componentDidMount() {
    this._fetchPosts();
  }

  _fetchPosts = () => postStore.fetchPosts(this.key, this.qs);

  getImageUri = item => {
    if (item.featured_media === 0) return null;

    let image = item.featured_media_url;
    const featuredMedia = item._embedded["wp:featuredmedia"][0];
    if (!image && featuredMedia.media_details.sizes.medium) {
      image = featuredMedia.media_details.sizes.medium.source_url;
    }
    if (!image) image = featuredMedia.source_url;
    if (!image) {
      image = featuredMedia.media_details.sizes.full.source_url;
    }

    return image;
  };

  render() {
    let _flex = this.props.flex;
    let _toRender = null;
    let _onPress = null;

    if (postStore.loading.get(this.key)) {
      //loading
      _onPress = null;
      _toRender = <ActivityIndicator color={colors.white} />;
    } else {
      //loaded
      _onPress = this.props.onPress;

      let item = postStore.getFirstItem(this.key);
      if (item) {
        let image = this.getImageUri(item);
        if (image) {
          _toRender = (
            <Image source={{ uri: image }} styleName="fill-parent">
              <Overlay styleName="fill-parent clear">
                <View styleName="vertical v-end fill-parent">
                  <View style={{ backgroundColor: "rgba(0,0,0,0.7)", padding: 3 }}>
                    <Text style={{ color: colors.white, fontSize: 9 }}>
                      {entities.decode(item.title.rendered)}
                    </Text>
                  </View>
                </View>
              </Overlay>
            </Image>
          );
        }

        if (_toRender === null) {
          _toRender = (
            <View styleName="center">
              <ScaledImageAsset source={require("../images/footer-icon-news.png")} width={50} />
              <Caption style={{ color: colors.white }} styleName="sm-gutter-top">
                Berita
              </Caption>
            </View>
          );
        }
      }
    }

    return (
      <TouchableOpacity
        style={{ flex: _flex, backgroundColor: colors.blue, margin: 1 }}
        onPress={_onPress}
      >
        <View styleName="center">{_toRender}</View>
      </TouchableOpacity>
    );
  }
}

HomeButtonBerita.propTypes = {
  flex: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired
};
