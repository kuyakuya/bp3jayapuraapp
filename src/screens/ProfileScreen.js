import React, { Component } from "react";
import { StyleSheet, ActivityIndicator } from "react-native";
import {
  Screen,
  View,
  Button,
  Image,
  Row,
  Title,
  Text,
  TouchableOpacity,
  Caption,
  Divider
} from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import { Linking } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import firebase from "react-native-firebase";
import { observer } from "mobx-react/native";
import { userStore } from "../stores";
import { colors } from "../theme";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import ScaleImageAsset from "../components/ScaledImageAsset";
import ImagePicker from "react-native-image-crop-picker";

const diameter = 120;
const styles = {
  container: {
    borderRadius: 100,
    height: diameter,
    width: diameter,
    backgroundColor: "#0072bb",
    borderColor: "#ffca08",
    borderWidth: 3,
    overflow: "hidden"
  }
};

class RowIcon extends Component {
  render() {
    const { icon, text } = this.props;
    return (
      <View
        styleName="horizontal md-gutter-vertical v-center"
        style={{ borderBottomWidth: 1, borderColor: colors.lightGray, marginHorizontal: 16 }}
      >
        <Icon name={icon} color={colors.lightGray} size={24} style={{ marginHorizontal: 8 }} />
        <Text>{text}</Text>
      </View>
    );
  }
}

@observer
export default class ProfileScreen extends Component {
  componentDidMount() {
    userStore.getProfile();
  }

  handleAvatarClicked = () => {
    if (userStore.loading) return;

    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      cropperCircleOverlay: true
    })
      .then(image => {
        userStore.updateAvatar(userStore.currentProfile.ID, image);
      })
      .catch(err => {});
  };

  render() {
    const profile = userStore.currentProfile;
    let _renderProfile = null;
    if (userStore.loading)
      _renderProfile = (
        <View styleName="lg-gutter-top">
          <ActivityIndicator color={colors.gray} />
        </View>
      );
    else if (!profile)
      _renderProfile = (
        <View styleName="vertical center lg-gutter" style={{ height: 200 }}>
          <Text styleName="md-gutter-bottom" style={{ color: colors.red }}>
            Load profile error!
          </Text>
          <TouchableOpacity onPress={() => userStore.getProfile()}>
            <Text style={{ textDecorationLine: "underline" }}>Retry</Text>
          </TouchableOpacity>
        </View>
      );
    else
      _renderProfile = (
        <View styleName="vertical">
          <RowIcon icon="portrait" text={profile.nama_depan} />
          <RowIcon icon="portrait" text={profile.nama_belakang} />
          <RowIcon icon="wc" text={profile.jenis_kelamin} />
          <RowIcon icon="home" text={profile.alamat} />
          <RowIcon icon="location-city" text={profile.tempat_lahir} />
          <RowIcon icon="event" text={profile.tanggal_lahir} />
          <RowIcon icon="favorite-border" text={profile.agama} />
          <RowIcon icon="school" text={profile.asal_sekolah} />
          <RowIcon icon="school" text={profile.jurusan} />
          <RowIcon icon="smartphone" text={profile.phone} />
          <RowIcon icon="mail-outline" text={profile.email} />
        </View>
      );

    const user = userStore.currentUser;
    let httpsAvatar = profile ? profile.avatar.replace("http://", "https://") : "";

    return (
      <Screen styleName="paper">
        <View styleName="vertical h-center sm-gutter" style={{ backgroundColor: colors.green }}>
          <TouchableOpacity style={styles.container} onPress={() => this.handleAvatarClicked()}>
            {profile ? (
              <Image styleName="medium-avatar" source={{ uri: httpsAvatar }} />
            ) : (
              <ScaleImageAsset source={require("../images/default-avatar.png")} width={120} />
            )}
          </TouchableOpacity>
          <Title styleName="sm-gutter-top" style={{ color: "#ffffff" }}>
            {profile ? `${profile.nama_depan} ${profile.nama_belakang}` : user.user_display_name}
          </Title>
          <Caption style={{ color: "#ffffff" }}>{user.user_email}</Caption>
          <View styleName="horizontal md-gutter-top">
            <Button
              style={{ marginHorizontal: 5, flex: 1 }}
              onPress={() => {
                if (profile) Actions.push("edit-profile");
              }}
            >
              <Icon name="create" color={colors.white} style={{ marginRight: 5 }} />
              <Caption style={{ color: colors.white }}>EDIT PROFIL</Caption>
            </Button>
            <Button
              style={{ marginHorizontal: 5, flex: 1 }}
              onPress={() => {
                if (profile) Actions.push("change-password");
              }}
            >
              <Icon name="vpn-key" color={colors.white} style={{ marginRight: 5 }} />
              <Caption style={{ color: colors.white }}>UBAH PASSWORD</Caption>
            </Button>
          </View>
        </View>
        <KeyboardAwareScrollView>{_renderProfile}</KeyboardAwareScrollView>
      </Screen>
    );
  }
}
