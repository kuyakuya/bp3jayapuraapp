import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Row, TouchableOpacity, Text, Image, Title, Caption } from "@shoutem/ui";
import { ActivityIndicator } from "react-native";
import { postStore } from "../stores";
import { observer } from "mobx-react/native";
import config from "../config";
import Entities from "html-entities";
import { colors } from "../theme";
import ScaledImageAsset from "../components/ScaledImageAsset";

const entities = new Entities.AllHtmlEntities();

@observer
export default class HomeButtonPengumuman extends Component {
  componentWillMount() {
    this.key = "pengumuman";
    this.qs = { categories: config.PENGUMUMAN_CATEGORY_ID };
  }

  componentDidMount() {
    this._fetchPosts();
  }

  _fetchPosts = () => postStore.fetchPosts(this.key, this.qs);

  render() {
    let _flex = this.props.flex;
    let _toRender = null;
    let _onPress = null;

    if (postStore.loading.get(this.key)) {
      //loading
      _onPress = null;
      _toRender = (
        <View styleName="center">
          <ActivityIndicator color={colors.white} />
        </View>
      );
    } else {
      //loaded
      _onPress = this.props.onPress;

      const arr = postStore.getItemsArray(this.key);
      if (arr.length > 0) {
        let list = arr.slice(0, 2).map(item => {
          //console.log(item);
          let title = entities.decode(item.title.rendered);
          return (
            <Text key={item.id} style={{ fontSize: 10, color: colors.white }}>
              &rsaquo; {title}
            </Text>
          );
        });

        _toRender = (
          <View styleName="vertical v-start">
            <Text style={{ color: colors.white, fontWeight: "bold" }}>Pengumuman</Text>
            <View styleName="sm-gutter-top">{list}</View>
          </View>
        );
      } else {
        //no content
        _toRender = (
          <View styleName="center">
            <ScaledImageAsset source={require("../images/footer-icon-pengumuman.png")} width={50} />
            <Caption style={{ color: colors.white }}>Pengumuman</Caption>
          </View>
        );
      }
    }

    return (
      <TouchableOpacity
        style={{ flex: _flex, backgroundColor: colors.darkGreen, margin: 1 }}
        onPress={_onPress}
      >
        <View styleName="fill-parent vertical v-start sm-gutter">{_toRender}</View>
      </TouchableOpacity>
    );
  }
}

HomeButtonPengumuman.propTypes = {
  flex: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired
};
