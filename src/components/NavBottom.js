import React, { Component } from "react";
import { Dimensions } from "react-native";
import { View, Overlay, Image, Title, TouchableOpacity } from "@shoutem/ui";
import NavBottomButton from "./NavBottomButton";
import { Actions } from "react-native-router-flux";

class NavBottom extends Component {
  reset = name => {
    Actions.reset(name);
  };

  replace = name => {
    Actions.replace(name);
  };

  render() {
    return (
      <View styleName="horizontal">
        <NavBottomButton
          imageUri={require("../images/footer-icon-home.png")}
          rightBorder
          caption="Beranda"
          onPress={() => this.reset("home")}
          selected={Actions.currentScene == "home" ? 1 : 0}
        />
        <NavBottomButton
          imageUri={require("../images/footer-icon-calendar.png")}
          rightBorder
          caption="Kalendar"
          onPress={() => this.replace("jadwal")}
          selected={Actions.currentScene == "jadwal" ? 1 : 0}
        />
        <NavBottomButton
          imageUri={require("../images/footer-icon-pengumuman.png")}
          rightBorder
          caption="Pengumuman"
          onPress={() => this.replace("pengumuman")}
          selected={Actions.currentScene == "pengumuman" ? 1 : 0}
        />
        <NavBottomButton
          imageUri={require("../images/footer-icon-news.png")}
          rightBorder
          caption="Berita"
          onPress={() => this.replace("berita")}
          selected={Actions.currentScene == "berita" ? 1 : 0}
        />
        <NavBottomButton
          imageUri={require("../images/footer-icon-pengaduan.png")}
          caption="Pengaduan"
          onPress={() => this.replace("pengaduan")}
          selected={Actions.currentScene == "pengaduan" ? 1 : 0}
        />
      </View>
    );
  }
}

export default NavBottom;
