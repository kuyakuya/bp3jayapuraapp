import config from "../config";
import axios from "axios";
import moment from "moment";
import { cacheAdapterEnhancer } from "axios-extensions";

shouldRefreshProfile = false;

const CACM_API_URL = config.API_URL + "/cacm/v1";

const axiosWithCache = axios.create({
  baseURL: "/",
  headers: { "Cache-Control": "no-cache" },
  // cache will be enabled by default
  adapter: cacheAdapterEnhancer(axios.defaults.adapter, true)
});

const withQuery = (url, params = {}) => {
  const esc = encodeURIComponent;
  let query = Object.keys(params)
    .filter(key => params[key] !== "" && params[key] !== null)
    .map(key => `${esc(key)}=${esc(params[key])}`)
    .join("&");
  query = query.length > 0 ? `?${query}` : null;

  return `${url}${query}`;
};

export const getCalendar = (params = {}) => {
  const defParams = {};
  const url = withQuery(`${CACM_API_URL}/calendar`, { ...defParams, ...params });
  console.log(url);

  return axiosWithCache.get(url);
};

export const getPrograms = (params = {}) => {
  let date = moment().format("YYYY-MM-DD");
  const defParams = { start_date: date };
  const url = withQuery(`${CACM_API_URL}/programs`, { ...defParams, ...params });
  console.log(url);

  return axiosWithCache.get(url);
};

export const sendPengaduan = (_name, _email, _phone, _subject, _message) => {
  const url = `${CACM_API_URL}/contact`;
  console.log(url);

  return axios.post(url, {
    nama: _name,
    email: _email,
    phone: _phone,
    subject: _subject,
    pesan: _message
  });
};

export const sendPendaftaran = params => {
  const url = `${CACM_API_URL}/register`;
  //const url = 'https://requestb.in/13d2p7j1';
  console.log(url);

  let formData = new FormData();
  for (const [key, value] of Object.entries(params)) {
    formData.append(key, value);
  }

  /*
	axios.interceptors.request.use(request => {
		console.log('Starting Request', request)
		return request
	})
	
	axios.interceptors.response.use(response => {
		console.log('Response:', response)
		return response
	})
	*/

  return axios({
    url: url,
    method: "POST",
    data: formData,
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"
    }
  });
};

export const getProfile = id => {
  const url = `${CACM_API_URL}/profile?email=${id}`;
  console.log(url);
  if (shouldRefreshProfile) {
    shouldRefreshProfile = false;
    return axiosWithCache.get(url, { forceUpdate: true });
  } else {
    return axiosWithCache.get(url);
  }
};

export const updateProfile = params => {
  const url = `${CACM_API_URL}/profile`;
  console.log(url);

  let formData = new FormData();
  for (const [key, value] of Object.entries(params)) {
    //if (value.length > 0)
    formData.append(key, value);
  }

  shouldRefreshProfile = true;
  return axios({
    url: url,
    method: "POST",
    data: formData,
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"
    }
  });
};

export const updateAvatar = (id, imageFile) => {
  const url = `${CACM_API_URL}/profile`;
  console.log(url);

  let filename = imageFile.path.replace(/^.*[\\\/]/, "");
  let ext = filename.split(".").pop();

  const formData = new FormData();
  formData.append("id", id);
  formData.append("avatar", {
    uri: imageFile.path,
    type: imageFile.mime,
    name: `avatar-${id}.${ext}`
  });

  /*
  axios.interceptors.request.use(request => {
    console.log("Starting Request", request);
    return request;
  });

  axios.interceptors.response.use(response => {
    console.log("Response:", response);
    return response;
  });
  */

  shouldRefreshProfile = true;
  return axios({
    url: url,
    method: "POST",
    data: formData,
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"
    }
  });
};
