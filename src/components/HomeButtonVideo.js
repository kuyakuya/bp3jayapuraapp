import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Row, TouchableOpacity, Text, Image } from "@shoutem/ui";
import { colors } from "../theme";
import ScaleImageAsset from "../components/ScaledImageAsset";

export default class HomeButtonVideo extends Component {
  render() {
    const { flex, onPress } = this.props;

    return (
      <TouchableOpacity
        style={{ flex: flex, backgroundColor: colors.green, margin: 1, padding: 10 }}
        onPress={onPress}
      >
        <View styleName="center">
          <ScaleImageAsset source={require("../images/icon-videogallery.png")} width={60} />
          <Text style={{ color: colors.white, marginTop: 10, fontSize: 12 }}>Video Gallery</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

HomeButtonVideo.propTypes = {
  flex: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired
};
