import _ from "lodash";
import { INCLUDE, changeColorAlpha } from "@shoutem/theme";
import getTheme from "@shoutem/ui/theme";
import { Dimensions } from "react-native";

const window = Dimensions.get("window");

const colors = {
  blue: "#0072bb",
  orange: "#f68121",
  yellow: "#ffca08",
  green: "#4db748",
  darkGreen: "#277E2F",
  white: "#ffffff",
  whiteSmoke: "#f5f5f5",
  black: "#333333",
  gray: "#808080",
  darkGray: "#a9a9a9",
  lightGray: "#d3d3d3",
  red: "#cc0000"
};

const overrideTheme = {
  ...getTheme(),
  inputStyle: {
    marginTop: 0,
    paddingVertical: 0
  },
  dropdownStyle: {
    horizontalContainer: {
      height: 36,
      padding: 8
    },
    selectedOption: {
      height: 16,
      padding: 0,
      "shoutem.ui.Text": {
        width: "85%",
        textAlign: "left"
      }
    }
  },
  "shoutem.ui.Button": {
    ".blue": {
      backgroundColor: colors.blue
    },
    ".orange": {
      backgroundColor: colors.orange
    },
    ".circle": {
      borderRadius: 500,
      paddingRight: 0
    },
    backgroundColor: colors.green,
    padding: 3,
    borderRadius: 0,
    borderWidth: 0,
    "shoutem.ui.Text": {
      color: colors.white,
      fontSize: 14
    }
  },
  "shoutem.ui.View": {
    ".pageContent": {
      padding: 8
    },
    ".content": {
      padding: 16
    },
    ".center": {
      alignItems: "center",
      justifyContent: "center",
      [INCLUDE]: ["fillParent"]
    }
  },
  "shoutem.ui.Html": {
    container: {
      padding: 16
    }
  },
  "shoutem.ui.TextInput": {
    height: 36,
    padding: 8
  },
  "shoutem.ui.DropDownMenu": {
    [INCLUDE]: ["dropdownStyle"],
    ".compact": {
      [INCLUDE]: ["dropdownStyle"],
      selectedOption: {
        "shoutem.ui.Text": {
          fontSize: 13
        }
      }
    }
  },
  "shoutem.ui.Overlay": {
    ".clear": {
      backgroundColor: "transparent"
    }
  }
};

const navbarStyles = {
  bg: {
    backgroundColor: colors.green,
    borderBottomColor: "transparent",
    elevation: 0,
    borderBottomWidth: 0
  },
  left: {
    color: colors.white
  },
  title: {
    color: colors.white
  }
};

//merge default theme with ours
const theme = _.merge(getTheme(), overrideTheme);
export { theme, navbarStyles, colors };
