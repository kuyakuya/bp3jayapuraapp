import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Screen, View, Text, Image, TouchableOpacity } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/MaterialIcons";
import Lightbox from "../components/BaseLightbox";

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default class ViewPhotoModal extends Component {
  render() {
    const { image } = this.props;
    return (
      <Lightbox verticalPercent={0.4} horizontalPercent={1.0}>
        <Image style={{ width: "100%" }} styleName="featured" source={image} />
      </Lightbox>
    );
  }
}
