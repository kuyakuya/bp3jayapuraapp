import React, { Component } from "react";
import { ActivityIndicator } from "react-native";
import { Screen, View, Button, Text, FormGroup, Caption, Title } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import TextInputWithIcon from "../components/TextInputWithIcon";
import DropDownWithArrow from "../components/DropDownWithArrow";
import DatePickerInput from "../components/DatePickerInput";
import { Alert } from "react-native";
import { userStore } from "../stores";
import { observer } from "mobx-react/native";
import { colors } from "../theme";
import Icon from "react-native-vector-icons/MaterialIcons";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import firebase from "react-native-firebase";

const genders = [
  { title: "Pilih Jenis Kelamin", value: "" },
  { title: "Laki-Laki", value: "laki-laki" },
  { title: "Perempuan", value: "perempuan" }
];

@observer
export default class EditProfileScreen extends Component {
  constructor(props) {
    super(props);

    const profile = userStore.currentProfile;
    const gender = genders.find(idx => idx.value === profile.jenis_kelamin);
    this.state = {
      nama_depan: profile.nama_depan,
      nama_belakang: profile.nama_belakang,
      alamat: profile.alamat,
      tempat_lahir: profile.tempat_lahir,
      tanggal_lahir: profile.tanggal_lahir,
      agama: profile.agama,
      asal_sekolah: profile.asal_sekolah,
      jurusan: profile.jurusan,
      phone: profile.phone,
      gender: gender
    };
    userStore.setUpdateSuccess(false);
  }

  error(msg) {
    Alert.alert("Error", msg);
  }

  inputIsValid() {
    if (!this.state.nama_depan) {
      alert("Nama depan wajib diisi");
      return false;
    }
    if (!this.state.gender || this.state.gender.value === "") {
      alert("Jenis kelamin harus diisi");
      return false;
    }
    if (!this.state.alamat) {
      alert("Alamat harus diisi");
      return false;
    }
    if (!this.state.tempat_lahir || !this.state.tanggal_lahir) {
      alert("Tempat dan tanggal lahir harus diisi");
      return false;
    }
    if (!this.state.agama) {
      alert("Keterangan agama harus diisi");
      return false;
    }
    if (!this.state.asal_sekolah || !this.state.jurusan) {
      alert("Asal sekolah dan jurusan harus diisi");
      return false;
    }
    if (!this.state.phone) {
      alert("Nomor telpon harus diisi");
      return false;
    }

    return true;
  }

  submitForm() {
    if (userStore.loading) return;
    if (!this.inputIsValid()) return;

    const params = {
      id: userStore.currentProfile.ID,
      nama_depan: this.state.nama_depan,
      nama_belakang: this.state.nama_belakang,
      phone: this.state.phone,
      alamat: this.state.alamat,
      jenis_kelamin: this.state.gender.value,
      tempat_lahir: this.state.tempat_lahir,
      tanggal_lahir: this.state.tanggal_lahir,
      agama: this.state.agama,
      asal_sekolah: this.state.asal_sekolah,
      jurusan: this.state.jurusan
    };

    firebase.analytics().logEvent("update_profile", params);
    userStore.updateProfile(params);
  }

  renderSpinner = () =>
    userStore.loading ? (
      <ActivityIndicator color={colors.white} style={{ marginLeft: 10 }} />
    ) : null;

  componentDidUpdate() {
    if (userStore.updateSuccess) {
      firebase.analytics().logEvent("update_profile_success");
      Alert.alert("Info", "Update berhasil", [{ text: "OK", onPress: () => Actions.pop() }]);
    }
  }

  render() {
    const profile = userStore.currentProfile;
    const gender = genders.find(idx => idx.value === profile.jenis_kelamin);
    return (
      <Screen styleName="paper">
        <KeyboardAwareScrollView>
          <FormGroup style={{ padding: 16 }}>
            <TextInputWithIcon
              icon="portrait"
              defaultValue={profile.nama_depan}
              placeholder={"Nama Depan"}
              onChangeText={text => this.setState({ nama_depan: text })}
            />
            <TextInputWithIcon
              icon="portrait"
              defaultValue={profile.nama_belakang}
              placeholder={"Nama Belakang"}
              onChangeText={text => this.setState({ nama_belakang: text })}
            />
            <DropDownWithArrow
              icon="wc"
              value={profile.jenis_kelamin}
              options={genders}
              selectedOption={this.state.gender ? this.state.gender : gender ? gender : genders[0]}
              onOptionSelected={selected => this.setState({ gender: selected })}
              titleProperty="title"
              valueProperty="value"
            />
            <TextInputWithIcon
              icon="home"
              defaultValue={profile.alamat}
              placeholder={"Alamat Lengkap"}
              onChangeText={text => this.setState({ alamat: text })}
            />
            <TextInputWithIcon
              icon="location-city"
              defaultValue={profile.tempat_lahir}
              placeholder={"Kota Kelahiran"}
              onChangeText={text => this.setState({ tempat_lahir: text })}
            />
            <DatePickerInput
              icon="event"
              value={profile.tanggal_lahir}
              placeholder={"Tanggal Lahir"}
              date={this.state.tanggal_lahir}
              mode="date"
              onDateChange={date => this.setState({ tanggal_lahir: date })}
            />
            <TextInputWithIcon
              icon="favorite-border"
              defaultValue={profile.agama}
              placeholder={"Agama"}
              onChangeText={text => this.setState({ agama: text })}
            />
            <TextInputWithIcon
              icon="school"
              defaultValue={profile.asal_sekolah}
              placeholder={"Asal Sekolah / Universitas"}
              onChangeText={text => this.setState({ asal_sekolah: text })}
            />
            <TextInputWithIcon
              icon="school"
              defaultValue={profile.jurusan}
              placeholder={"Jurusan"}
              onChangeText={text => this.setState({ jurusan: text })}
            />
            <TextInputWithIcon
              icon="smartphone"
              defaultValue={profile.phone}
              placeholder={"No Telepon / HP"}
              keyboardType="phone-pad"
              onChangeText={text => this.setState({ phone: text })}
            />
          </FormGroup>
        </KeyboardAwareScrollView>
        <Button styleName="blue" onPress={() => this.submitForm()}>
          <Text>UPDATE</Text>
          {this.renderSpinner()}
        </Button>
      </Screen>
    );
  }
}
