import { observable, action } from "mobx";
import { sendPendaftaran, getProfile, updateProfile, updateAvatar } from "../services/cacmApi";
import { getJwtToken } from "../services/wordpressApi";
import { Alert, AsyncStorage } from "react-native";

const STORAGE_KEY = "@bp3jayapura:key";

export default class UserStore {
  @observable currentUser = null; //{ user_email: 'anu', user_display_name:'Udin', token: ''};
  @observable loading = false;
  @observable registerSuccess = false;
  @observable loginSuccess = false;
  @observable updateSuccess = false;
  @observable currentProfile = null;

  async loadUser() {
    try {
      let user = JSON.parse(await AsyncStorage.getItem(STORAGE_KEY));
      console.log("load user from storage:", user);
      this.setUser(user);
      if (user) this.getProfile();
    } catch (e) {
      console.log(e);
    }
  }

  async login(email, password) {
    this.setLoading(true);
    this.setLoginSuccess(false);
    try {
      const response = await getJwtToken(email, password);
      //console.log(response.data);
      let user = response.data;
      try {
        await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(user));
      } catch (e) {
        //console.log(e);
        throw new Error(e.message);
      }

      //load profile
      const response2 = await getProfile(user.user_email);
      //console.log(response.data);
      if (response2.data.cacm.code !== 200) {
        throw new Error(response2.data.cacm.message);
      }
      this.setProfile(response2.data.cacm.data);
      //all good..now set in session
      this.setUser(user);
      this.setLoginSuccess(true);
    } catch (e) {
      //console.log("error", e.response.data);
      if (e.response.data.message) {
        let err = e.response.data.message.replace(/<(?:.|\n)*?>/gm, "");
        Alert.alert("Error", err);
      } else Alert.alert("Error", e.message);
    } finally {
      this.setLoading(false);
    }
  }

  async logout() {
    this.setUser(null);
    this.setProfile(null);
    await AsyncStorage.removeItem(STORAGE_KEY);
  }

  async register(params = {}) {
    this.setLoading(true);
    this.setRegisterSuccess(false);
    try {
      const response = await sendPendaftaran(params);
      //console.log(response.data);
      if (response.data.cacm.code !== 200) {
        throw new Error(response.data.cacm.messages[0]);
      }
      this.setRegisterSuccess(true);
    } catch (e) {
      Alert.alert("Error", e.message);
    } finally {
      this.setLoading(false);
    }
  }

  async getProfile() {
    if (!this.isLoggedIn()) {
      Alert.alert("Error", "User not logged-in!");
      return;
    }
    this.setLoading(true);
    //this.setProfile(null);
    try {
      const response = await getProfile(this.currentUser.user_email);
      console.log(response.data);
      if (response.data.cacm.code !== 200) {
        throw new Error(response.data.cacm.message);
      }
      this.setProfile(response.data.cacm.data);
    } catch (e) {
      Alert.alert("Error", e.message);
    } finally {
      this.setLoading(false);
    }
  }

  async updateProfile(params = {}) {
    if (!this.isLoggedIn() || !this.currentProfile) {
      Alert.alert("Error", "User not logged-in!");
      return;
    }
    this.setLoading(true);
    this.setUpdateSuccess(false);
    try {
      const response = await updateProfile(params);
      //console.log(response.data);
      if (response.data.cacm.code !== 200) {
        throw new Error(response.data.cacm.messages[0]);
      }
      this.setProfile(response.data.cacm.data);
      this.setUpdateSuccess(true);
    } catch (e) {
      Alert.alert("Error", e.message);
    } finally {
      this.setLoading(false);
    }
  }

  async updateAvatar(id, image) {
    if (!this.isLoggedIn() || !this.currentProfile) {
      Alert.alert("Error", "User not logged-in!");
      return;
    }

    this.setLoading(true);
    this.setUpdateSuccess(false);
    try {
      const response = await updateAvatar(id, image);
      //console.log(response.data);
      if (response.data.cacm.code !== 200) {
        throw new Error(response.data.cacm.messages[0]);
      }
      this.setProfile(response.data.cacm.data);
      this.setUpdateSuccess(true);
    } catch (e) {
      Alert.alert("Error", e.message);
    } finally {
      this.setLoading(false);
    }
  }

  @action
  setLoading(loading) {
    this.loading = loading;
  }

  @action
  setUser(user) {
    this.currentUser = user;
  }

  @action
  setRegisterSuccess(success) {
    this.registerSuccess = success;
  }

  @action
  setLoginSuccess(success) {
    this.loginSuccess = success;
  }

  @action
  setUpdateSuccess(success) {
    this.updateSuccess = success;
  }

  @action
  setProfile(profile) {
    this.currentProfile = profile;
  }

  isLoggedIn() {
    return this.currentUser && this.currentUser.user_email.length > 0;
  }
}
