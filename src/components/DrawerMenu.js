import React from "react";
import { View, Button, Text } from "@shoutem/ui";
import Icon from "react-native-vector-icons/MaterialIcons";
import { TouchableOpacity } from "@shoutem/ui/components/TouchableOpacity";

export default class DrawerMenu extends React.Component {
  render() {
    const { icon, text, onPress } = this.props;
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{ height: 48, borderBottomWidth: 0, borderColor: "#dddddd" }}
      >
        <View styleName="fill-parent horizontal h-start v-center">
          <Icon name={icon} size={18} />
          <Text style={{ marginLeft: 10, fontSize: 18, flex: 1 }}>{text}</Text>
          <Icon name="keyboard-arrow-right" size={18} color="#dddddd" />
        </View>
      </TouchableOpacity>
    );
  }
}
