import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { View, Text, DropDownMenu } from "@shoutem/ui";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Actions } from "react-native-router-flux";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import { colors } from "../theme";

export default class DropDownWithArrow extends Component {
  render() {
    const { icon, selectedOption, compact } = this.props;
    const _icon = icon ? (
      <Icon name={icon} color={colors.lightGray} size={24} style={{ marginHorizontal: 8 }} />
    ) : null;

    let _styleName = compact ? "compact" : "";

    return (
      <View
        styleName="horizontal v-center"
        style={{
          padding: 0,
          borderBottomWidth: 1,
          borderColor: colors.lightGray,
          marginBottom: 10
        }}
      >
        {_icon}
        <DropDownMenu {...this.props} styleName={_styleName} />
      </View>
    );
  }
}
