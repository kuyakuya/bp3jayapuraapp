import React, { Component } from "react";
import { Image } from "react-native";
import PropTypes from "prop-types";

export default class ScaledImageAsset extends Component {
  componentWillMount() {
    const { width, height } = Image.resolveAssetSource(this.props.source);
    if (this.props.width && !this.props.height) {
      this.setState({ width: this.props.width, height: height * (this.props.width / width) });
    } else if (!this.props.width && this.props.height) {
      this.setState({ width: width * (this.props.height / height), height: this.props.height });
    } else {
      this.setState({ width: width, height: height });
    }
  }

  render() {
    return (
      <Image
        source={this.props.source}
        style={{ height: this.state.height, width: this.state.width }}
      />
    );
  }
}

ScaledImageAsset.propTypes = {
  //source: PropTypes.number.isRequired,
  width: PropTypes.number,
  height: PropTypes.number
};
