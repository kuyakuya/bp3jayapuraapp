import React, { Component } from "react";
import { View, Row, Spinner } from "@shoutem/ui";

class RowLoading extends Component {
  render = () => (
    <Row>
      <View styleName="horizontal h-center">
        <Spinner />
      </View>
    </Row>
  );
}

export default RowLoading;
