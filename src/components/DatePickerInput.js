import React, { Component } from "react";
import { StyleSheet, Alert } from "react-native";
import { View, Text, TextInput, TouchableOpacity } from "@shoutem/ui";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Actions } from "react-native-router-flux";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import DatePicker from "react-native-datepicker";
import { colors } from "../theme";

export default class DatePickerInput extends Component {
  render() {
    const { icon, value, placeholder, onDateChange } = this.props;
    const _icon = icon ? (
      <Icon name={icon} color={colors.lightGray} size={24} style={{ marginHorizontal: 8 }} />
    ) : null;

    return (
      <View
        styleName="horizontal v-center"
        style={{
          padding: 0,
          borderBottomWidth: 1,
          borderColor: colors.lightGray,
          marginBottom: 12
        }}
      >
        {_icon}
        <DatePicker
          style={{ flex: 1, borderWidth: 0 }}
          date={value}
          mode="date"
          format="DD-MM-YYYY"
          minDate="01-01-1980"
          maxDate="01-01-2020"
          placeholder={placeholder}
          showIcon={false}
          confirmBtnText="OK"
          cancelBtnText="Batal"
          customStyles={{
            placeholderText: {
              color: "#aaaaaa",
              fontSize: 15,
              fontFamily: "Rubik-Regular"
            },
            dateInput: {
              borderWidth: 0,
              alignItems: "flex-start",
              marginLeft: 8
            },
            dateText: {
              fontSize: 15,
              fontFamily: "Rubik-Regular"
            }
          }}
          onDateChange={onDateChange}
        />
      </View>
    );
  }
}
