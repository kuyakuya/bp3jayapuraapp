import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Row, TouchableOpacity, Text, Image, Title } from "@shoutem/ui";
import { colors } from "../theme";
import ScaleImageAsset from "../components/ScaledImageAsset";

export default class HomeButtonPendaftaran extends Component {
  render() {
    const { flex, onPress } = this.props;

    return (
      <TouchableOpacity
        style={{ flex: flex, backgroundColor: colors.blue, margin: 1 }}
        onPress={onPress}
      >
        <View styleName="fill-parent horizontal h-end v-center sm-gutter-right">
          <ScaleImageAsset source={require("../images/dashboard-maskot.png")} height={100} />
        </View>
        <View styleName="fill-parent vertical md-gutter" style={{ paddingRight: 40 }}>
          <Title style={{ lineHeight: 20, color: colors.yellow }} styleName="bold">
            Daftar Online BP3Jayapura
          </Title>
          <Text style={{ fontSize: 11, lineHeight: 12, color: colors.white, marginTop: 5 }}>
            Seleksi Penerimaan Calon Taruna/i Balai Pendidikan dan Pelatihan Penerbangan Jayapura
            Tahun 2018
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

HomeButtonPendaftaran.propTypes = {
  flex: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired
};
