import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Screen, View, Button, Image, Heading, Subtitle, Row, Title, Text } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import { Linking } from "react-native";
import NavHeader from "../components/NavHeader";
import Icon from "react-native-vector-icons/MaterialIcons";
import firebase from "react-native-firebase";

export default class KontakScreen extends Component {
  callNumber = number => {
    let url = "tel:" + number;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        console.log("calling " + url);
        firebase.analytics().logEvent("call_contact_number");
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  sendEmail = to => {
    let url = "mailto:" + to;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        console.log("emailing " + url);
        firebase.analytics().logEvent("email_contact");
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  render() {
    const email = "info@cac-jayapura.ac.id";
    return (
      <Screen styleName="paper">
        <NavHeader compact title="Kontak" />
        <View styleName="vertical md-gutter">
          <Heading styleName="bold">
            Hubungi kami jika anda membutuhkan bantuan atau informasi
          </Heading>
          <View styleName="vertical lg-gutter-top">
            <Title>Jl. Kayubatu No.6</Title>
            <Title>Tanjung Ria</Title>
            <Title>Jayapura, 99117</Title>
          </View>
          <View styleName="horizontal v-center space-between lg-gutter-top">
            <View styleName="vertical v-center">
              <Text>{email}</Text>
              <Text style={{ marginTop: 8 }}>(0967) 541049</Text>
            </View>
            <Button
              styleName="circle orange"
              style={{ padding: 10 }}
              onPress={() => {
                this.callNumber("0967541049");
              }}
            >
              <Icon name="phone" size={32} color="white" />
            </Button>
            <Button
              styleName="circle orange"
              style={{ padding: 10 }}
              onPress={() => {
                this.sendEmail(email);
              }}
            >
              <Icon name="email" size={32} color="white" />
            </Button>
          </View>
        </View>
      </Screen>
    );
  }
}
