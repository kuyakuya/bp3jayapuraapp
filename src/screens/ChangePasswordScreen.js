import React, { Component } from "react";
import { ScrollView, ActivityIndicator, KeyboardAvoidingView } from "react-native";
import {
  Screen,
  View,
  Button,
  Text,
  FormGroup,
  DropDownMenu,
  Divider,
  Caption,
  Title,
  Subtitle,
  Card
} from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import TextInputWithIcon from "../components/TextInputWithIcon";
import DropDownWithArrow from "../components/DropDownWithArrow";
import DatePickerInput from "../components/DatePickerInput";
import { Alert } from "react-native";
import { userStore } from "../stores";
import { observer } from "mobx-react/native";
import { colors } from "../theme";
import Icon from "react-native-vector-icons/MaterialIcons";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import firebase from "react-native-firebase";

@observer
export default class ChangePasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      repeatPassword: ""
    };
    userStore.setUpdateSuccess(false);
  }

  error(msg) {
    Alert.alert("Error", msg);
  }

  inputIsValid() {
    if (!this.state.password) {
      alert("Password harap diisi!");
      return false;
    }
    if (this.state.password.length < 6) {
      alert("Password minimal 6 karakter!");
      return false;
    }
    if (this.state.password != this.state.repeatPassword) {
      alert("Password yg diulang tidak identik!");
      return false;
    }
    return true;
  }

  submitForm() {
    if (userStore.loading) return;
    if (!this.inputIsValid()) return;

    //console.log(userStore.currentProfile);
    const params = {
      id: userStore.currentProfile.ID,
      pass1: this.state.password,
      pass2: this.state.repeatPassword
    };

    firebase.analytics().logEvent("update_password", params);
    userStore.updateProfile(params);
  }

  renderSpinner = () =>
    userStore.loading ? (
      <ActivityIndicator color={colors.white} style={{ marginLeft: 10 }} />
    ) : null;

  componentDidUpdate(prevProps, prevState) {
    if (userStore.updateSuccess) {
      firebase.analytics().logEvent("update_password_success");
      Alert.alert("Info", "Update berhasil", [{ text: "OK", onPress: () => Actions.pop() }]);
    }
  }

  render() {
    return (
      <Screen styleName="paper">
        {/*<NavHeader compact title='Form Pendaftaran'/>*/}
        <KeyboardAwareScrollView>
          <FormGroup style={{ padding: 16 }}>
            <TextInputWithIcon
              icon="vpn-key"
              secureTextEntry
              placeholder={"Password baru"}
              onChangeText={text => this.setState({ password: text })}
            />
            <TextInputWithIcon
              icon="vpn-key"
              secureTextEntry
              placeholder={"Password baru diulang"}
              onChangeText={text => this.setState({ repeatPassword: text })}
            />
          </FormGroup>
        </KeyboardAwareScrollView>
        <Button styleName="blue" onPress={() => this.submitForm()}>
          <Text>UPDATE</Text>
          {this.renderSpinner()}
        </Button>
        {/*<NavBottom/>*/}
      </Screen>
    );
  }
}
