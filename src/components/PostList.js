import React, { Component } from "react";
import { Screen, View, ListView, Text, Row, Spinner } from "@shoutem/ui";
import { postStore } from "../stores";
import { observer } from "mobx-react/native";
import { Actions } from "react-native-router-flux";
import PostListItem from "./PostListItem";
import RowLoading from "./RowLoading";

@observer
export default class PostList extends Component {
  componentWillMount() {
    this.key = this.props.storeKey || "all";
    this.qs = this.props.queryString || {};
    this.isBerita = this.props.isBerita || false;
    this._fetchPosts();
  }

  _fetchPosts = () => postStore.fetchPosts(this.key, this.qs);

  handleRefresh = () => {
    postStore.setPage(this.key, 1);
    postStore.deleteItems(this.key);
    this._fetchPosts();
  };

  handleLoadMore = () => {
    if (postStore.loading.get(this.key)) return;

    postStore.nextPage(this.key);
    this._fetchPosts();
  };

  renderHeader = () => {
    return null;
  };

  renderRow = item => {
    return (
      <View>
        <PostListItem
          item={item}
          isBerita={this.isBerita}
          onPress={() => {
            Actions.push("berita-detail", { item: item, isBerita: this.isBerita });
          }}
        />
      </View>
    );
  };

  render() {
    return (
      <ListView
        data={postStore.getItemsArray(this.key)}
        renderRow={this.renderRow}
        renderHeader={this.renderHeader}
        onLoadMore={this.handleLoadMore}
        onRefresh={this.handleRefresh}
        loading={postStore.loading.get(this.key)}
      />
    );
  }
}
