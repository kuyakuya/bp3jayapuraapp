import React, { Component } from "react";
import { Router, Drawer, Scene, Modal, Lightbox, Actions } from "react-native-router-flux";
import { StyleProvider } from "@shoutem/theme";
import { observer } from "mobx-react/native";
import { theme, navbarStyles } from "./src/theme";
import {
  HomeScreen,
  BeritaScreen,
  BeritaDetailScreen,
  PengumumanScreen,
  GaleriFotoScreen,
  GaleriVideoScreen,
  PendaftaranScreen,
  InformasiScreen,
  KontakScreen,
  JadwalScreen,
  PengaduanScreen,
  LoginScreen,
  ViewPhotoModal,
  CalendarsScreen,
  DrawerContent,
  AboutScreen,
  InfoPendaftaranScreen,
  JadwalPendaftaranScreen,
  ProfileScreen,
  EditProfileScreen,
  ChangePasswordScreen
} from "./src/screens";
import firebase from "react-native-firebase";

export default class App extends Component {
  onEnter = () => {
    //console.log(Actions.currentScene);
    firebase.analytics().setCurrentScreen(Actions.currentScene, Actions.currentScene);
  };

  render() {
    return (
      <StyleProvider style={theme}>
        <Router
          wrapBy={observer}
          navigationBarStyle={navbarStyles.bg}
          navBarButtonColor="#fff"
          leftButtonTextStyle={navbarStyles.left}
          titleStyle={navbarStyles.title}
        >
          <Drawer key="drawer" contentComponent={DrawerContent} hideDrawerButton hideNavBar>
            <Lightbox key="lightbox">
              <Scene key="root">
                <Scene
                  key="home"
                  component={HomeScreen}
                  title="Home"
                  hideNavBar
                  initial
                  onEnter={this.onEnter}
                />
                <Scene
                  key="login"
                  component={LoginScreen}
                  title="Login"
                  onEnter={this.onEnter}
                  back
                />
                <Scene
                  key="berita"
                  component={BeritaScreen}
                  title="Berita"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="berita-detail"
                  component={BeritaDetailScreen}
                  title="Berita"
                  onEnter={this.onEnter}
                  back
                />
                <Scene
                  key="pengumuman"
                  component={PengumumanScreen}
                  title="Pengumuman"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="pendaftaran"
                  component={PendaftaranScreen}
                  title="Pendaftaran"
                  onEnter={this.onEnter}
                  back
                />
                <Scene
                  key="galeri-foto"
                  component={GaleriFotoScreen}
                  title="Galeri Foto"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="galeri-video"
                  component={GaleriVideoScreen}
                  title="Galeri Video"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="jadwal"
                  component={JadwalScreen}
                  title="Jadwal Akademi"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="pengaduan"
                  component={PengaduanScreen}
                  title="Form Pengaduan"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="kontak"
                  component={KontakScreen}
                  title="Kontak"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="about"
                  component={AboutScreen}
                  title="About"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="info-pendaftaran"
                  component={InfoPendaftaranScreen}
                  title="Info Pendaftaran"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="jadwal-pendaftaran"
                  component={JadwalPendaftaranScreen}
                  title="Jadwal Pendaftaran"
                  onEnter={this.onEnter}
                  hideNavBar
                />
                <Scene
                  key="profile"
                  component={ProfileScreen}
                  title=""
                  onEnter={this.onEnter}
                  back
                />
                <Scene
                  key="edit-profile"
                  component={EditProfileScreen}
                  title="Edit Profile"
                  onEnter={this.onEnter}
                  back
                />
                <Scene
                  key="change-password"
                  component={ChangePasswordScreen}
                  title="Ubah Password"
                  onEnter={this.onEnter}
                  back
                />
              </Scene>
              <Scene key="view-photo" component={ViewPhotoModal} hideNavBar />
            </Lightbox>
          </Drawer>
        </Router>
      </StyleProvider>
    );
  }
}
