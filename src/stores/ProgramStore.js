import { observable, action } from "mobx";
import { getPrograms } from "../services/cacmApi";
import { Alert } from "react-native";

export default class ProgramStore {
  @observable loading = false;
  @observable items = [];

  async fetchPrograms(params = {}) {
    this.setLoading(true);
    try {
      const response = await getPrograms();
      //console.log(response.data);
      if (response.data.cacm.code !== 200) {
        throw new Error(response.data.cacm.messages[0]);
      }
      this.updateItems(response.data.cacm.data);
    } catch (e) {
      Alert.alert("Error", e.message);
    } finally {
      this.setLoading(false);
    }
  }

  getItems() {
    return this.items.peek();
  }

  @action
  setLoading(loading) {
    this.loading = loading;
  }

  @action
  updateItems(items) {
    this.items = items;
    this.items.unshift({ title: "Pilih Program", program_id: 0, diklat: [] });
  }

  @action
  deleteItems() {
    this.items.clear();
  }
}
