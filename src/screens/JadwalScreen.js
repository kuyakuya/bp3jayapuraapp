import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Screen, View, Button, Image, Text, Spinner, Caption } from "@shoutem/ui";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import { CalendarList, Agenda } from "react-native-calendars";
import { calendarStore } from "../stores";
import { observer } from "mobx-react/native";
import moment from "moment";
import { colors } from "../theme";
import { Actions } from "react-native-router-flux";

@observer
export default class JadwalScreen extends Component {
  componentWillMount() {
    this._fetchPosts();
  }

  _fetchPosts = () => calendarStore.fetchEvents();

  renderAgendaItem = (item, isFirstItem) => {
    //console.log(item);
    let topMargin = 2;
    if (isFirstItem === true) topMargin = 15;

    return (
      <View
        style={{ marginTop: topMargin, marginRight: 5, padding: 10, backgroundColor: "#f5f5f5" }}
      >
        <Text>{item.title}</Text>
        <Caption>
          {item.start} &rarr; {item.end}
        </Caption>
      </View>
    );
  };

  renderDay = (day, item) => {
    let _toRender = null;
    if (day == undefined) {
      _toRender = null;
    } else {
      _toRender = (
        <Text style={{ fontSize: 16 }} styleName="h-center bold">
          {moment(day.dateString).format("DD MMM")}
        </Text>
      );
    }

    return <View style={{ marginTop: 15, width: 50, padding: 5 }}>{_toRender}</View>;
  };

  renderEmptyData = () => {
    return (
      <View styleName="vertical fill-parent v-center h-center">
        <Text>Tidak Ada Event</Text>
      </View>
    );
  };

  render() {
    let _toRender = null;

    if (calendarStore.loading) {
      _toRender = (
        <View styleName="vertical v-center h-center fill-parent">
          <Spinner />
        </View>
      );
    } else {
      let markedDates = {};
      let agendaItems = {};

      const obj = calendarStore.getItems();
      for (let date in obj) {
        let value = obj[date].map(o => {
          return (o = Object.assign(o, { start: date }));
        });
        markedDates[date] = { marked: true };
        agendaItems[date] = value;
      }
      //console.log(agendaItems);

      _toRender = (
        <Agenda
          items={agendaItems}
          markedDates={markedDates}
          renderItem={this.renderAgendaItem}
          renderEmptyData={this.renderEmptyData}
          rowHasChanged={(r1, r2) => {
            return r1.text !== r2.text;
          }}
          renderDay={this.renderDay}
          renderEmptyDate={() => {
            return <View />;
          }}
          theme={{
            backgroundColor: colors.white,
            calendarBackground: colors.white,
            dotColor: colors.blue
          }}
        />
      );
    }

    return (
      <Screen styleName="paper">
        <NavHeader compact title="Kalendar" />
        <View styleName="pageContent" style={{ flex: 1 }}>
          {_toRender}
        </View>
        <NavBottom />
      </Screen>
    );
  }
}
