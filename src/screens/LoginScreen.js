import React, { Component } from "react";
import { Dimensions, Keyboard, ScrollView, ActivityIndicator } from "react-native";
import { Screen, View, FormGroup, Button, Text, Image, Overlay } from "@shoutem/ui";
import TextInputWithIcon from "../components/TextInputWithIcon";
import { Actions } from "react-native-router-flux";
import ScaledImageAsset from "../components/ScaledImageAsset";
import { Alert } from "react-native";
import { userStore } from "../stores";
import { colors } from "../theme";
import { observer } from "mobx-react/native";
import firebase from "react-native-firebase";

const window = Dimensions.get("window");
const styles = {
  container: {
    flex: 1,
    padding: 0
  }
};

@observer
export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //email: 'rarofef@ugimail.net',
      //password: 'eIFWGBzEF496',
      email: "",
      password: "",
      hideBottomImage: false
    };

    userStore.setLoginSuccess(false);
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ hideBottomImage: true });
  };

  _keyboardDidHide = () => {
    this.setState({ hideBottomImage: false });
  };

  error(msg) {
    Alert.alert("Error", msg);
  }

  inputIsValid() {
    if (!this.state.email) {
      alert("Alamat email harus diisi");
      return false;
    }
    if (!this.state.password) {
      alert("Password harus diisi");
      return false;
    }
    return true;
  }

  submitForm() {
    if (userStore.loading) return;
    if (!this.inputIsValid()) return;

    firebase.analytics().logEvent("login", { email: this.state.email });
    userStore.login(this.state.email, this.state.password);
  }

  showRegister = () => {
    Actions.push("pendaftaran");
  };

  renderSpinner = () =>
    userStore.loading === true ? (
      <ActivityIndicator color={colors.white} style={{ marginLeft: 10 }} />
    ) : null;

  renderSoruTvai = () => {
    {
      /* trick so form wont resize */
    }
    if (this.state.hideBottomImage) return <View style={{ width: window.width }} />;
    else
      return (
        <ScaledImageAsset source={require("../images/bp3-soru-tvai.png")} width={window.width} />
      );
  };

  componentDidUpdate(prevProps, prevState) {
    if (userStore.loginSuccess) {
      userStore.setLoginSuccess(false);
      firebase.analytics().logEvent("login_success");
      Alert.alert("Info", "Login berhasil", [{ text: "OK", onPress: () => Actions.reset("home") }]);
    }
  }

  render() {
    return (
      <Screen styleName="">
        <Image source={require("../images/login-bg.jpg")} width={window.width}>
          <Overlay style={{ backgroundColor: "transparent", padding: 0 }}>
            <View styleName="vertical v-start" style={styles.container}>
              <ScrollView>
                <View styleName="vertical h-center lg-gutter-top">
                  <ScaledImageAsset
                    source={require("../images/bp3-logo-with-title.png")}
                    width={window.width * 0.85}
                  />
                </View>
                <FormGroup style={{ flex: 1, marginVertical: 30, paddingHorizontal: 30 }}>
                  <TextInputWithIcon
                    icon="email"
                    placeholder={"Email"}
                    transparent
                    keyboardType="email-address"
                    onChangeText={text => this.setState({ email: text })}
                  />
                  <TextInputWithIcon
                    icon="lock"
                    placeholder={"Password"}
                    transparent
                    secureTextEntry
                    onChangeText={text => this.setState({ password: text })}
                  />
                  <Button
                    styleName="blue"
                    onPress={() => this.submitForm()}
                    style={{ marginTop: 10 }}
                  >
                    <Text>LOGIN</Text>
                    {this.renderSpinner()}
                  </Button>
                  <Button styleName="orange" onPress={this.showRegister} style={{ marginTop: 5 }}>
                    <Text>DAFTAR</Text>
                  </Button>
                </FormGroup>
                {this.renderSoruTvai()}
              </ScrollView>
            </View>
          </Overlay>
        </Image>
      </Screen>
    );
  }
}
