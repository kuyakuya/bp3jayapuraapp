import React, { Component } from "react";
import { Screen, View } from "@shoutem/ui";
import PostList from "../components/PostList";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import config from "../config";
import firebase from "react-native-firebase";

export default class PengumumanScreen extends Component {
  render() {
    return (
      <Screen styleName="">
        <NavHeader compact title="Pengumuman" />
        <View styleName="pageContent" style={{ flex: 1 }}>
          <PostList
            storeKey="pengumuman"
            queryString={{ categories: config.PENGUMUMAN_CATEGORY_ID }}
            isBerita={false}
          />
        </View>
        <NavBottom />
      </Screen>
    );
  }
}
