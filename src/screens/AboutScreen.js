import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Screen, View, Card, Title, Text, Caption } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import { Linking } from "react-native";
import NavHeader from "../components/NavHeader";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class AboutScreen extends Component {
  openURL = url => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        console.log("calling " + url);
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  render() {
    const email = "info@catc-jayapura.or.id";
    return (
      <Screen styleName="paper">
        <NavHeader compact title="Tentang Aplikasi" />
        <View styleName="flexible vertical md-gutter">
          <Card style={{ marginTop: 16, width: "100%" }}>
            <Title>Tentang</Title>
            <Caption textAlign="justify">
              BP3 Jayapura Online adalah aplikasi untuk umum, siswa dan internal Balai Pendidikan
              dan Pelatihan Penerbangan Jayapura. Dengan wilayah kerja Propinsi Maluku dan Iria
              mmaka BP3 Jayapura menjadi sekolah rujukan untuk pendidikan perihal penerbangan dan
              landasan udara di Indonesia Timur. BP3 Jayapura Online di harapkan menjadi saluran
              alternatif untuk menggali informasi mengenai BP3 Jayapura dan informasi akademik yang
              ada di BP3 Jayapura.
            </Caption>
          </Card>
          <Card style={{ marginTop: 16, width: "100%" }}>
            <Title>Developed By</Title>
            <Caption>Malayeka Digital Solution</Caption>
          </Card>
          <Card style={{ marginTop: 16, width: "100%" }}>
            <Title>Feedback / Laporkan Bug</Title>
            <Caption>Email di support@malayeka.id</Caption>
          </Card>
          {/* 
          <Card style={{ marginTop: 16, width: "100%" }}>
            <Title>Development Tools</Title>
            <Caption>
              React Native, Shoutem UI, RN Router Flux, Axios, RN Calendars, RN DatePicker, RN
              Render Html
            </Caption>
          </Card>
          */}
        </View>
      </Screen>
    );
  }
}
