import React from "react";
import { StyleSheet, Alert } from "react-native";
import { View, Image, Overlay, Divider } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import DrawerMenu from "../components/DrawerMenu";
import ScaleImageAsset from "../components/ScaledImageAsset";
import { observer } from "mobx-react/native";
import { userStore } from "../stores";
import firebase from "react-native-firebase";

@observer
export default class DrawerContent extends React.Component {
  logout() {
    userStore.logout();
    Actions.drawerClose();
    firebase.analytics().logEvent("logout");
    Alert.alert("Info", "Anda berhasil logout.");
  }

  renderAuthMenu = () => {
    if (!userStore.isLoggedIn())
      return <DrawerMenu icon="lock-outline" text="Login" onPress={() => Actions.push("login")} />;
    else return <DrawerMenu icon="exit-to-app" text="Logout" onPress={() => this.logout()} />;
  };

  render() {
    return (
      <View>
        <Image
          style={{ width: window.width, height: 170 }}
          source={require("../images/nav-gradient.png")}
        >
          <Overlay style={{ backgroundColor: "transparent", flex: 1, justifyContent: "center" }}>
            <ScaleImageAsset source={require("../images/bp3-logo-with-title.png")} height={45} />
          </Overlay>
        </Image>
        <View styleName="md-gutter-horizontal">
          <DrawerMenu icon="home" text="Beranda" onPress={() => Actions.reset("home")} />
          {/*
          <DrawerMenu
            icon="user-profile"
            text="Profile"
            onPress={() => (userStore.isLoggedIn ? Actions.push("profile") : Actions.push("login"))}
          />
          */}
          <DrawerMenu icon="description" text="Berita" onPress={() => Actions.push("berita")} />
          <DrawerMenu icon="event" text="Jadwal" onPress={() => Actions.push("jadwal")} />
          <DrawerMenu
            icon="mail-outline"
            text="Kontak Kami"
            onPress={() => Actions.push("kontak")}
          />
          <DrawerMenu
            icon="info-outline"
            text="Tentang Aplikasi"
            onPress={() => Actions.push("about")}
          />
          <Divider styleName="line" />
          {this.renderAuthMenu()}
        </View>
      </View>
    );
  }
}
