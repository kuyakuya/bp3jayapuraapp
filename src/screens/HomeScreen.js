import React, { Component } from "react";
import { StyleSheet, Dimensions } from "react-native";
import { Screen, View } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import HomeButtonJadwal from "../components/HomeButtonJadwal";
import HomeButtonPengumuman from "../components/HomeButtonPengumuman";
import HomeButtonBerita from "../components/HomeButtonBerita";
import HomeButtonPendaftaran from "../components/HomeButtonPendaftaran";
import HomeButtonPhoto from "../components/HomeButtonPhoto";
import HomeButtonPengaduan from "../components/HomeButtonPengaduan";
import HomeButtonVideo from "../components/HomeButtonVideo";
import HomeButton from "../components/HomeButton";
import NavHeaderWithCarousel from "../components/NavHeaderWithCarousel";
import { userStore } from "../stores";
import { colors } from "../theme";
import { observer } from "mobx-react/native";
import firebase from "react-native-firebase";

@observer
export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    if (!userStore.isLoggedIn()) userStore.loadUser();
  }

  componentDidMount() {}

  goTo = name => {
    Actions.push(name);
  };

  render() {
    return (
      <Screen styleName="paper">
        <NavHeaderWithCarousel />
        <View style={{ flex: 1, padding: 1 }}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <HomeButtonJadwal flex={2} onPress={() => this.goTo("jadwal")} />
            <HomeButtonPengumuman flex={1} onPress={() => this.goTo("pengumuman")} />
          </View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <HomeButtonBerita flex={1} onPress={() => this.goTo("berita")} />
            {!userStore.isLoggedIn() ? (
              <HomeButtonPendaftaran flex={2} onPress={() => this.goTo("pendaftaran")} />
            ) : (
              <View style={{ flex: 2, flexDirection: "row" }}>
                <HomeButton
                  title="Info Pendaftaran"
                  color={colors.green}
                  flex={1}
                  icon="info-outline"
                  onPress={() => this.goTo("info-pendaftaran")}
                />
                <HomeButton
                  title="Jadwal Sipencatar"
                  color={colors.blue}
                  textColor={colors.white}
                  flex={1}
                  icon="event"
                  onPress={() => this.goTo("jadwal-pendaftaran")}
                />
              </View>
            )}
          </View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <HomeButtonPhoto flex={1} onPress={() => this.goTo("galeri-foto")} />
            <HomeButtonPengaduan flex={1} onPress={() => this.goTo("pengaduan")} />
            <HomeButtonVideo flex={1} onPress={() => this.goTo("galeri-video")} />
          </View>
        </View>
      </Screen>
    );
  }
}
