import React, { Component } from "react";
import { ActivityIndicator } from "react-native";
import {
  Screen,
  View,
  Button,
  Text,
  FormGroup,
  Divider,
  Caption,
  Title,
  Subtitle,
  Card
} from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import TextInputWithIcon from "../components/TextInputWithIcon";
import DropDownWithArrow from "../components/DropDownWithArrow";
import DatePickerInput from "../components/DatePickerInput";
import { Alert } from "react-native";
import { userStore, programStore } from "../stores";
import { observer } from "mobx-react/native";
import { colors } from "../theme";
import Icon from "react-native-vector-icons/MaterialIcons";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import firebase from "react-native-firebase";

const genders = [
  { title: "Pilih Jenis Kelamin", value: "" },
  { title: "Laki-Laki", value: "laki-laki" },
  { title: "Perempuan", value: "perempuan" }
];

@observer
export default class Pendaftaran extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      address: "",
      kotaLahir: "",
      tanggalLahir: "",
      agama: "",
      sekolah: "",
      jurusan: "",
      phone: "",
      email: "",
      gender: "",
      program: ""
    };
    userStore.setRegisterSuccess(false);
  }

  componentWillMount() {
    this._fetchPrograms();
  }

  _fetchPrograms = () => programStore.fetchPrograms();

  error(msg) {
    Alert.alert("Error", msg);
  }

  inputIsValid() {
    if (!this.state.firstName) {
      alert("Nama depan wajib diisi");
      return false;
    }
    if (!this.state.gender || this.state.gender.value === "") {
      alert("Jenis kelamin harus diisi");
      return false;
    }
    if (!this.state.address) {
      alert("Alamat harus diisi");
      return false;
    }
    if (!this.state.kotaLahir || !this.state.tanggalLahir) {
      alert("Tempat dan tanggal lahir harus diisi");
      return false;
    }
    if (!this.state.agama) {
      alert("Keterangan agama harus diisi");
      return false;
    }
    if (!this.state.sekolah || !this.state.jurusan) {
      alert("Asal sekolah dan jurusan harus diisi");
      return false;
    }
    if (!this.state.phone) {
      alert("Nomor telpon harus diisi");
      return false;
    }
    if (!this.state.program || this.state.program.program_id === 0) {
      alert("Pilihan program/diklat harus diisi");
      return false;
    }
    if (!this.state.email) {
      alert("Alamat email harus diisi");
      return false;
    }

    return true;
  }

  submitForm() {
    if (userStore.loading || programStore.loading) return;
    if (!this.inputIsValid()) return;

    Alert.alert("Konfirmasi", "Lanjut mengirim data pendaftaran ?", [
      { text: "Lanjut", onPress: () => this.realSubmitForm() },
      { text: "Batal", style: "cancel" }
    ]);
  }

  realSubmitForm() {
    const params = {
      user_email: this.state.email,
      first_name: this.state.firstName,
      last_name: this.state.lastName,
      program_id: this.state.program.program_id,
      diklat_id: 0,
      phone: this.state.phone,
      alamat: this.state.address,
      jenis_kelamin: this.state.gender.value,
      tempat_lahir: this.state.kotaLahir,
      tanggal_lahir: this.state.tanggalLahir,
      agama: this.state.agama,
      asal_sekolah: this.state.sekolah,
      jurusan: this.state.jurusan
    };

    firebase.analytics().logEvent("send_pendaftaran", params);
    userStore.register(params);
  }

  renderDiklatInfo = () => {
    const diklatArr = this.state.program.diklat;
    if (diklatArr && diklatArr.length > 0) {
      return (
        <View styleName="vertical sm-gutter">
          {diklatArr.map(diklat => (
            <Card
              key={diklat.title}
              style={{
                width: "100%",
                borderWidth: 1,
                borderColor: colors.lightGray,
                padding: 5,
                borderRadius: 5
              }}
            >
              <Title style={{ color: colors.gray }}>Info Diklat</Title>
              <Caption>{diklat.title}</Caption>
              <Caption>
                {diklat.start} &rarr; {diklat.end}
              </Caption>
            </Card>
          ))}
        </View>
      );
    } else return null;
  };

  renderSpinner = () =>
    userStore.loading || programStore.loading ? (
      <ActivityIndicator color={colors.white} style={{ marginLeft: 10 }} />
    ) : null;

  renderAlreadyRegistered = () => (
    <Screen styleName="paper">
      <View styleName="center">
        <Icon name="info" color={colors.orange} size={128} styleName="lg-gutter-bottom" />
        <Title>Anda Sudah Terdaftar</Title>
        <Subtitle styleName="h-center md-gutter-horizontal">
          Tidak perlu melakukan pendaftaran kembali :) Jika ini adalah kesalahan, silahkan Logout
          dulu kemudian mendaftar ulang.
        </Subtitle>
        <View styleName="horizontal md-gutter">
          <Button styleName="clear lg-gutter-right" onPress={() => Actions.reset("home")}>
            <Icon name="home" size={16} />
            <Text style={{ color: colors.black }}>Beranda</Text>
          </Button>
          <Button styleName="clear" onPress={() => this.logout()}>
            <Icon name="exit-to-app" size={16} />
            <Text style={{ color: colors.black }}>Logout</Text>
          </Button>
        </View>
      </View>
    </Screen>
  );

  renderRegistrationSuccess = () => (
    <Screen styleName="paper">
      <View styleName="center">
        <Icon name="done" color={colors.green} size={128} styleName="lg-gutter-bottom" />
        <Title>Pendaftaran Berhasil</Title>
        <Subtitle styleName="h-center md-gutter-horizontal">
          Silahkan cek email anda untuk konfirmasi pendaftaran dan mendapatkan password.
        </Subtitle>
        <View styleName="horizontal md-gutter">
          <Button styleName="clear lg-gutter-right" onPress={() => Actions.reset("home")}>
            <Icon name="home" size={16} />
            <Text style={{ color: colors.black }}>Beranda</Text>
          </Button>
          <Button styleName="clear" onPress={() => Actions.replace("login")}>
            <Icon name="input" size={16} />
            <Text style={{ color: colors.black }}>Login</Text>
          </Button>
        </View>
      </View>
    </Screen>
  );

  componentDidUpdate(prevProps, prevState) {
    if (userStore.registerSuccess) {
      firebase.analytics().logEvent("send_pendaftaran_success");
    }
  }

  render() {
    let programs = [{ title: "Pilih Program", program_id: 0, diklat: [] }];
    if (!programStore.loading) programs = programStore.getItems();

    if (userStore.isLoggedIn()) {
      return this.renderAlreadyRegistered();
    }

    //done submitting?
    if (userStore.registerSuccess) {
      return this.renderRegistrationSuccess();
    }

    return (
      <Screen styleName="paper">
        {/*<NavHeader compact title='Form Pendaftaran'/>*/}
        <KeyboardAwareScrollView>
          <FormGroup style={{ padding: 16 }}>
            <TextInputWithIcon
              icon="portrait"
              placeholder={"Nama Depan"}
              onChangeText={text => this.setState({ firstName: text })}
            />
            <TextInputWithIcon
              icon="portrait"
              placeholder={"Nama Belakang"}
              onChangeText={text => this.setState({ lastName: text })}
            />
            <DropDownWithArrow
              icon="wc"
              options={genders}
              selectedOption={this.state.gender ? this.state.gender : genders[0]}
              onOptionSelected={selected => this.setState({ gender: selected })}
              titleProperty="title"
              valueProperty="value"
            />
            <TextInputWithIcon
              icon="home"
              placeholder={"Alamat Lengkap"}
              onChangeText={text => this.setState({ address: text })}
            />
            <TextInputWithIcon
              icon="location-city"
              placeholder={"Kota Kelahiran"}
              onChangeText={text => this.setState({ kotaLahir: text })}
            />
            <DatePickerInput
              icon="event"
              placeholder={"Tanggal Lahir"}
              date={this.state.tanggalLahir}
              mode="date"
              onDateChange={date => this.setState({ tanggalLahir: date })}
            />
            <TextInputWithIcon
              icon="favorite-border"
              placeholder={"Agama"}
              onChangeText={text => this.setState({ agama: text })}
            />
            <TextInputWithIcon
              icon="school"
              placeholder={"Asal Sekolah / Universitas"}
              onChangeText={text => this.setState({ sekolah: text })}
            />
            <TextInputWithIcon
              icon="school"
              placeholder={"Jurusan"}
              onChangeText={text => this.setState({ jurusan: text })}
            />
            <TextInputWithIcon
              icon="smartphone"
              placeholder={"No Telepon / HP"}
              keyboardType="phone-pad"
              onChangeText={text => this.setState({ phone: text })}
            />
            <TextInputWithIcon
              icon="mail-outline"
              placeholder={"Email"}
              keyboardType="email-address"
              onChangeText={text => this.setState({ email: text })}
            />
            <DropDownWithArrow
              icon="local-library"
              options={programs}
              selectedOption={this.state.program ? this.state.program : programs[0]}
              onOptionSelected={selected => {
                this.setState({ program: selected });
              }}
              titleProperty="title"
              valueProperty="program_id"
              compact
            />
            {this.renderDiklatInfo()}
            {/* add padding supaya bisa scroll ke bawah */}
            <Divider />
            <Divider />
            <Divider />
          </FormGroup>
        </KeyboardAwareScrollView>
        <Button styleName="blue" onPress={() => this.submitForm()}>
          <Text>DAFTAR</Text>
          {this.renderSpinner()}
        </Button>
        {/*<NavBottom/>*/}
      </Screen>
    );
  }
}
