import React, { Component } from "react";
import PropTypes from "prop-types";
import { Dimensions } from "react-native";
import { View, Overlay, Image, Title, Text } from "@shoutem/ui";
import ButtonAvatar from "./ButtonAvatar";
import { TouchableOpacity } from "@shoutem/ui/components/TouchableOpacity";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Actions } from "react-native-router-flux";
import { colors } from "../theme";
import { observer } from "mobx-react/native";
import { userStore } from "../stores";
import Carousel from "react-native-looped-carousel";
import ScaledImageAsset from "../components/ScaledImageAsset";

const window = Dimensions.get("window");

@observer
export default class NavHeader extends Component {
  showDrawer = () => {
    Actions.drawerOpen();
  };

  showLogin = () => {
    Actions.push("login");
  };

  showProfile = () => {
    Actions.push("profile");
  };

  renderAvatar = () => {
    if (!userStore.isLoggedIn()) return <ButtonAvatar auth={false} onPress={this.showLogin} />;
    else {
      const profile = userStore.currentProfile;
      return (
        <ButtonAvatar
          auth={true}
          avatar={profile && profile.avatar ? profile.avatar : null}
          fullname={profile && profile.name_depan ? profile.nama_depan : null}
          onPress={this.showProfile}
        />
      );
    }
  };

  render() {
    const { compact, title } = this.props;
    let imageHeight = null;
    let image = null;
    let _title = null;

    if (compact) {
      imageHeight = window.height * 0.27;
      imageUri = require("../images/nav-gradient.png");
      _title = (
        <View styleName="horizontal h-end" style={{}}>
          <Title style={{ color: colors.white }}>{title}</Title>
        </View>
      );
    } else {
      imageHeight = window.height * 0.4;
      imageUri = this.props.imageUri;

      return this.renderCarousel();
    }

    return (
      <Image style={{ width: window.width, height: imageHeight }} source={imageUri}>
        <Overlay styleName="fill-parent clear">
          <View styleName="fill-parent vertical v-start">
            <Image
              style={{ width: window.width, height: 160 }}
              resizeMode="stretch"
              source={require("../images/header.png")}
            />
          </View>
          <View style={{ position: "absolute", right: 5, bottom: 5 }}>{_title}</View>
          {/* hamburger icon */}
          <View style={{ position: "absolute", top: 20, left: 10, padding: 5 }}>
            <TouchableOpacity onPress={this.showDrawer}>
              <Icon name="menu" size={36} color={colors.white} />
            </TouchableOpacity>
          </View>
          {/* avatar */}
          <View style={{ position: "absolute", top: 20, right: 20 }}>{this.renderAvatar()}</View>
        </Overlay>
      </Image>
    );
  }
}

NavHeader.propTypes = {
  compact: PropTypes.bool,
  title: PropTypes.string,
  imageUri: PropTypes.number
};
