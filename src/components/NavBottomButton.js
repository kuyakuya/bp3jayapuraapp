import React, { Component } from "react";
import { View, Row, TouchableOpacity, Text, Image } from "@shoutem/ui";

class NavBottomButton extends Component {
  render() {
    const { imageUri, caption, rightBorder, selected } = this.props;
    let borderWidth = rightBorder ? 1 : 0;
    let _onPress = selected === 1 ? null : this.props.onPress;
    let _bgColor = selected === 1 ? "#cc6e22" : "#f3842b";

    return (
      <TouchableOpacity
        style={{
          flex: 1,
          backgroundColor: _bgColor,
          height: 70,
          borderRightWidth: borderWidth,
          borderColor: "#ffffff"
        }}
        onPress={_onPress}
      >
        <View styleName="vertical v-center h-center fill-parent">
          <Image source={imageUri} style={{ width: 30, height: 30 }} />
          <Text style={{ color: "#ffffff", marginTop: 5, fontSize: 10 }}>{caption}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default NavBottomButton;
