import React from "react";
import { StyleSheet } from "react-native";
import { View, Image } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import DrawerMenu from "../components/DrawerMenu";
import { TouchableOpacity, Text } from "@shoutem/ui";
import Icon from "react-native-vector-icons/MaterialIcons";
import ScaleImageAsset from "../components/ScaledImageAsset";

const diameter = 70;
const styles = {
  container: {
    borderRadius: 100,
    height: diameter,
    width: diameter,
    backgroundColor: "#0072bb",
    borderColor: "#ffca08",
    borderWidth: 3,
    overflow: "hidden"
  },
  loginText: {
    color: "#ffffff",
    fontSize: 11,
    paddingHorizontal: 10
  }
};

export default class ButtonAvatar extends React.Component {
  render() {
    const { auth, avatar, fullname, onPress } = this.props;
    let content = null;

    let avatarUri = avatar ? avatar.replace("http://", "https://") : null;

    let image = avatar ? (
      <Image styleName="small" source={{ uri: avatarUri }} />
    ) : (
      <ScaleImageAsset source={require("../images/default-avatar.png")} width={70} />
    );

    if (auth === false) {
      content = (
        <View styleName="fill-parent vertical v-center h-center">
          <Icon name="input" size={24} color="#ffffff" />
          <Text style={styles.loginText} numberOfLines={1}>
            Log In
          </Text>
        </View>
      );
    } else {
      content = (
        <View styleName="fill-parent vertical v-center h-center">
          {/*
          <Icon name="perm-identity" size={36} color="#ffffff" />
          <Text style={styles.loginText} numberOfLines={1}>
            {fullname}
          </Text>
          */}
          {image}
        </View>
      );
    }

    return (
      <TouchableOpacity style={styles.container} onPress={onPress}>
        {content}
      </TouchableOpacity>
    );
  }
}
