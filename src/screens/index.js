import BeritaScreen from "./BeritaScreen";
import BeritaDetailScreen from "./BeritaDetailScreen";
import PengumumanScreen from "./PengumumanScreen";
import GaleriFotoScreen from "./GaleriFotoScreen";
import GaleriVideoScreen from "./GaleriVideoScreen";
import HomeScreen from "./HomeScreen";
import AboutScreen from "./AboutScreen";
import KontakScreen from "./KontakScreen";
import PendaftaranScreen from "./PendaftaranScreen";
import JadwalScreen from "./JadwalScreen";
import PengaduanScreen from "./PengaduanScreen";
import LoginScreen from "./LoginScreen";
import ViewPhotoModal from "./ViewPhotoModal";
import DrawerContent from "./DrawerContent";
import InfoPendaftaranScreen from "./InfoPendaftaranScreen";
import JadwalPendaftaranScreen from "./JadwalPendaftaranScreen";
import ProfileScreen from "./ProfileScreen";
import EditProfileScreen from "./EditProfileScreen";
import ChangePasswordScreen from "./ChangePasswordScreen";

export {
  HomeScreen,
  BeritaScreen,
  BeritaDetailScreen,
  PengumumanScreen,
  GaleriFotoScreen,
  GaleriVideoScreen,
  AboutScreen,
  KontakScreen,
  PendaftaranScreen,
  JadwalScreen,
  PengaduanScreen,
  LoginScreen,
  ViewPhotoModal,
  DrawerContent,
  InfoPendaftaranScreen,
  JadwalPendaftaranScreen,
  ProfileScreen,
  EditProfileScreen,
  ChangePasswordScreen
};
