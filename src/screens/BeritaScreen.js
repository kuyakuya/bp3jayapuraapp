import React, { Component } from "react";
import { Screen, View } from "@shoutem/ui";
import PostList from "../components/PostList";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import config from "../config";

export default class BeritaScreen extends Component {
  render() {
    return (
      <Screen styleName="">
        <NavHeader compact title="Berita" />
        <View styleName="pageContent" style={{ flex: 1 }}>
          <PostList
            storeKey="berita"
            queryString={{ categories_exclude: config.PENGUMUMAN_CATEGORY_ID }}
            isBerita
          />
        </View>
        <NavBottom />
      </Screen>
    );
  }
}
