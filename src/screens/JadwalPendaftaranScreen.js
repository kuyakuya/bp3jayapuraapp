import React, { Component } from "react";
import { StyleSheet, ScrollView } from "react-native";
import { Screen, View, Text, Spinner, Title, Caption, Divider } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import NavHeader from "../components/NavHeader";
import Icon from "react-native-vector-icons/MaterialIcons";
import config from "../config";
import { pageStore } from "../stores";
import { observer } from "mobx-react/native";
import HTML from "react-native-render-html";
import Entities from "html-entities";
import moment from "moment";

const entities = new Entities.AllHtmlEntities();
const styles = {
  html: {
    h1: {
      marginHorizontal: 16,
      marginTop: 8,
      marginBottom: 8
    },
    p: {
      marginHorizontal: 16,
      marginTop: 8,
      marginBottom: 8
    },
    padding: 16
  }
};

@observer
export default class JadwalPendaftaranScreen extends Component {
  constructor(props) {
    super(props);
    this.id = config.JADWAL_SIPENCATAR_PAGE_ID;
    this._fetchPage();
  }

  _fetchPage = () => pageStore.fetchPage(this.id);

  alterNode = node => {
    let { name, attribs } = node;
    //change http -> https , also remove image attribs
    if (name && name === "img") {
      let src = node.attribs.src.replace("http://", "https://");
      node.attribs = [];
      node.attribs.src = src;
      //console.log(node);
      return node;
    }
  };

  onLinkPress = (evt, href) => {
    //console.log(href);
    Linking.openURL(href);
  };

  render() {
    let _toRender = null;
    if (pageStore.loading.get(this.id)) {
      _toRender = (
        <View styleName="center" style={{ flex: 1 }}>
          <Spinner />
        </View>
      );
    } else {
      _toRender = <Text>loaded</Text>;
      const data = pageStore.getContent(this.id);
      _toRender = (
        <ScrollView style={{ flex: 1, padding: 16 }}>
          <HTML
            htmlStyles={styles.html}
            html={data.content.rendered}
            alterNode={this.alterNode}
            onLinkPress={this.onLinkPress}
            imagesMaxWidth={window.width - 32}
          />
        </ScrollView>
      );
    }

    return (
      <Screen styleName="paper">
        <NavHeader compact title="Jadwal Sipencatar" />
        {_toRender}
      </Screen>
    );
  }
}
