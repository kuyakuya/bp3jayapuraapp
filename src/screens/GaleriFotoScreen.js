import React, { Component } from "react";
import { Linking } from "react-native";
import { Screen, View, Image, ListView, GridRow, TouchableOpacity, Divider } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import { galleryStore } from "../stores";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import { observer } from "mobx-react/native";
import config from "../config";

@observer
export default class GaleriFotoScreen extends Component {
  constructor(props) {
    super(props);
    this.key = "photos";
    this.params = { parent: config.MEDIA_PARENT_ID };
    this._fetchImages();
  }

  _fetchImages = () => galleryStore.fetchData(this.key, this.params);

  handleRefresh = () => {
    galleryStore.setPage(this.key, 1);
    galleryStore.deleteItems(this.key);
    this._fetchImages();
  };

  handleLoadMore = () => {
    if (galleryStore.loading.get(this.key)) return;
    if (galleryStore.moreData.get(this.key) === false) return;

    galleryStore.nextPage(this.key);
    this._fetchImages();
  };

  handleClick = url => {
    Actions.push("view-photo", { image: { uri: url } });
  };

  renderRow = (rowData, sectionId, index) => {
    const cellViews = rowData.map((item, id) => {
      let sizes = item.media_details.sizes;
      if (sizes.full === undefined || sizes.thumbnail === undefined) return null;

      return (
        <TouchableOpacity
          key={id}
          onPress={() => {
            this.handleClick(sizes.full.source_url);
          }}
        >
          <Image
            style={{ width: "100%" }}
            styleName="medium-square"
            source={{ uri: sizes.thumbnail.source_url }}
          />
        </TouchableOpacity>
      );
    });

    return <GridRow columns={3}>{cellViews}</GridRow>;
  };

  render() {
    const groupedData = GridRow.groupByRows(galleryStore.getItemsArray(this.key), 3, () => 1);

    return (
      <Screen styleName="paper">
        <NavHeader compact title="Galeri Foto" />
        <ListView
          data={groupedData}
          renderRow={this.renderRow}
          renderFooter={() => <Divider />}
          onLoadMore={this.handleLoadMore}
          onRefresh={this.handleRefresh}
          loading={galleryStore.loading.get(this.key)}
        />
        <NavBottom />
      </Screen>
    );
  }
}
