import React, { Component } from "react";
import { Linking } from "react-native";
import {
  Screen,
  View,
  Button,
  Image,
  Text,
  ListView,
  TouchableOpacity,
  Subtitle,
  Card,
  Caption,
  Video
} from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import { galleryStore } from "../stores";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import { observer } from "mobx-react/native";
import moment from "moment";

@observer
export default class GaleriVideoScreen extends Component {
  constructor(props) {
    super(props);
    this.key = "videos";
    this.qs = this.props.queryString || {};
    this._fetchImages();
  }

  _fetchImages = () => galleryStore.fetchData(this.key, this.qs);

  handleRefresh = () => {
    galleryStore.setPage(this.key, 1);
    galleryStore.deleteItems(this.key);
    this._fetchImages();
  };

  handleLoadMore = () => {
    if (galleryStore.loading.get(this.key)) return;
    if (galleryStore.moreData.get(this.key) === false) return;

    galleryStore.nextPage(this.key);
    this._fetchImages();
  };

  handleClick = url => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  renderRow = item => {
    let youtubeUrl = "https://www.youtube.com/watch?v=" + item.id.videoId;
    return (
      <Card style={{ marginTop: 8, width: "100%" }}>
        <View>
          <Video source={{ uri: youtubeUrl }} />
        </View>
        <View styleName="content">
          <Subtitle>{item.snippet.title}</Subtitle>
          <Caption>{moment(item.snippet.publishedAt).format("LLL")}</Caption>
        </View>
      </Card>
    );
  };

  render() {
    return (
      <Screen styleName="">
        <NavHeader compact title="Galeri Video" />
        <View styleName="pageContent" style={{ flex: 1 }}>
          <ListView
            data={galleryStore.getItemsArray(this.key)}
            renderRow={this.renderRow}
            renderFooter={() => null}
            onLoadMore={this.handleLoadMore}
            onRefresh={this.handleRefresh}
            loading={galleryStore.loading.get(this.key)}
          />
        </View>
        <NavBottom />
      </Screen>
    );
  }
}
