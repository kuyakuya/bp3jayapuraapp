import GalleryStore from "./GalleryStore";
import PostStore from "./PostStore";
import CalendarStore from "./CalendarStore";
import UserStore from "./UserStore";
import ProgramStore from "./ProgramStore";
import PageStore from "./PageStore";

const galleryStore = new GalleryStore();
const postStore = new PostStore();
const calendarStore = new CalendarStore();
const userStore = new UserStore();
const programStore = new ProgramStore();
const pageStore = new PageStore();

export { galleryStore, postStore, calendarStore, userStore, programStore, pageStore };
