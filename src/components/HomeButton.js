import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Row, TouchableOpacity, Text, Image } from "@shoutem/ui";
import { colors } from "../theme";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class HomeButton extends Component {
  render() {
    const { flex, onPress, color, textColor, icon, title } = this.props;

    return (
      <TouchableOpacity
        style={{ flex: flex, backgroundColor: color, margin: 1, padding: 10 }}
        onPress={onPress}
      >
        <View styleName="center">
          <Icon name={icon} size={60} color={textColor} />
          <Text style={{ color: textColor, marginTop: 0, fontSize: 12 }}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

HomeButton.defaultProps = {
  flex: 1,
  color: colors.blue,
  textColor: colors.white
};

HomeButton.propTypes = {
  title: PropTypes.string.isRequired,
  flex: PropTypes.number,
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  icon: PropTypes.string.isRequired,
  textColor: PropTypes.string
};
