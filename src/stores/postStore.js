import { observable, action } from "mobx";
import { getPosts } from "../services/wordpressApi";
import { Alert } from "react-native";

export default class PostStore {
  @observable loading = observable.map();
  @observable page = observable.map();
  @observable items = observable.map();

  async fetchPosts(key = "all", params = {}) {
    if (!this.page.get(key)) this.setPage(key, 1);

    this.setLoading(key, true);
    try {
      const response = await getPosts({ page: this.page.get(key), ...params });
      this.updateItems(key, response.data);
    } catch (e) {
      Alert.alert("Koneksi Error", e.message);
    } finally {
      this.setLoading(key, false);
    }
  }

  getItemsArray(key = "all") {
    //console.log("getItemsArray - "+ key);
    return this.items.has(key) ? this.items.get(key).peek() : [];
  }

  getFirstItem(key = "all") {
    return this.items.has(key) ? this.items.get(key).peek()[0] : null;
  }

  @action
  setLoading(key, loading) {
    //console.log("setLoading "+ loading);
    this.loading.set(key, loading);
  }

  @action
  setPage(key, page) {
    this.page.set(key, page);
  }

  @action
  nextPage(key) {
    this.page.set(key, this.page.get(key) + 1);
  }

  @action
  updateItems(key, items) {
    //console.log("updateItems - "+ key);
    const add = this.page.get(key) > 1;
    if (this.items.has(key)) {
      this.items.set(key, add ? [...this.items.get(key), ...items] : items);
    } else {
      this.items.set(key, items);
    }
  }

  @action
  deleteItems(key) {
    if (this.items.has(key)) {
      //console.log("deleteItems - "+ key);
      this.items.delete(key);
    }
  }
}
