import { observable, action } from "mobx";
import { getCalendar } from "../services/cacmApi";
import { Alert } from "react-native";

export default class CalendarStore {
  @observable loading = false;
  @observable page;
  items = [];

  async fetchEvents(params = {}) {
    if (!this.page) this.page = 1;
    this.setLoading(true);
    try {
      const response = await getCalendar({ page: this.page, ...params });
      //console.log(response.data);
      if (response.data.cacm.code !== 200) {
        throw new Error(response.data.cacm.messages[0]);
      }

      this.updateItems(response.data.cacm.data);
    } catch (e) {
      Alert.alert("Error", e.message);
    } finally {
      this.setLoading(false);
    }
  }

  getItems() {
    return this.items;
  }

  @action
  setLoading(loading) {
    this.loading = loading;
  }

  @action
  setPage(page) {
    this.page = page;
  }

  @action
  nextPage(key) {
    this.page = this.page + 1;
  }

  updateItems(items) {
    const add = this.page > 1;
    this.items = add ? [...this.items, ...items] : items;
  }

  deleteItems() {
    this.items.clear();
  }
}
