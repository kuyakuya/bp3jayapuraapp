import React, { Component } from "react";
import { ScrollView, ActivityIndicator } from "react-native";
import { Screen, View, Button, Text, FormGroup, Icon, DropDownMenu } from "@shoutem/ui";
import { Actions } from "react-native-router-flux";
import NavHeader from "../components/NavHeader";
import NavBottom from "../components/NavBottom";
import TextInputWithIcon from "../components/TextInputWithIcon";
import DropDownWithArrow from "../components/DropDownWithArrow";
import { sendPengaduan } from "../services/cacmApi";
import { Alert } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import firebase from "react-native-firebase";

export default class PengaduanScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      message: "",
      type: "",
      types: [{ title: "Saran", value: "saran" }, { title: "Pengaduan", value: "pengaduan" }],
      loading: false
    };
  }

  inputIsValid = () => {
    if (!this.state.name || !this.state.email || !this.state.phone || !this.state.message)
      return false;
    return true;
  };

  async submitForm() {
    if (this.state.loading) return;

    if (!this.inputIsValid()) {
      Alert.alert("Error", "Silahkan cek kembali inputan anda. Semua field harus diisi!");
      return;
    }

    firebase.analytics().logEvent("send_pengaduan", { name: this.state.name });

    this.setState({ loading: true });
    try {
      const { name, email, phone, message } = this.state;
      const type = this.state.type.title;
      const response = await sendPengaduan(name, email, phone, type, message);
      console.log(response.data);

      firebase.analytics().logEvent("send_pengaduan_success", { name: this.state.name });
      Alert.alert("Info", "Pesan berhasil dikirim.");

      //back to dashboard
      Actions.reset("home");
    } catch (e) {
      Alert.alert("Koneksi Error", e.message);
    } finally {
      this.setState({ loading: false });
    }
  }

  renderSpinner = () =>
    this.state.loading ? <ActivityIndicator color="#ffffff" style={{ marginLeft: 10 }} /> : null;

  render() {
    return (
      <Screen styleName="paper">
        <NavHeader compact title="Form Pengaduan" />
        <KeyboardAwareScrollView>
          <FormGroup style={{ padding: 16 }}>
            <TextInputWithIcon
              icon="portrait"
              placeholder={"Nama Lengkap"}
              onChangeText={text => this.setState({ name: text })}
            />
            <TextInputWithIcon
              icon="mail-outline"
              placeholder={"Alamat Email"}
              keyboardType="email-address"
              onChangeText={text => this.setState({ email: text })}
            />
            <TextInputWithIcon
              icon="smartphone"
              placeholder={"No Telpon"}
              keyboardType="phone-pad"
              onChangeText={text => this.setState({ phone: text })}
            />
            <DropDownWithArrow
              icon="chat-bubble-outline"
              options={this.state.types}
              selectedOption={this.state.type ? this.state.type : this.state.types[0]}
              onOptionSelected={selected => this.setState({ type: selected })}
              titleProperty="title"
              valueProperty="value"
            />
            <TextInputWithIcon
              placeholder={"Isi Pesan"}
              multiline={true}
              onChangeText={text => this.setState({ message: text })}
            />
            <Button styleName="blue" onPress={() => this.submitForm()}>
              <Text>KIRIM PESAN</Text>
              {this.renderSpinner()}
            </Button>
          </FormGroup>
        </KeyboardAwareScrollView>
        <NavBottom />
      </Screen>
    );
  }
}
