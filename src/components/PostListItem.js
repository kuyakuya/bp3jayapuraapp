import React, { Component } from "react";
import {
  View,
  ListView,
  Row,
  Divider,
  Image,
  Subtitle,
  Caption,
  TouchableOpacity,
  Card
} from "@shoutem/ui";
import moment from "moment";
import Entities from "html-entities";

const entities = new Entities.AllHtmlEntities();

export default class PostListItem extends Component {
  renderThumbnail = () => {
    const { item } = this.props;
    if (item.featured_media === 0) return null;

    let image = item.featured_media_url;
    const featuredMedia = item._embedded["wp:featuredmedia"][0];
    /*
    if (!image && featuredMedia.media_details.sizes.thumbnail) {
      image = featuredMedia.media_details.sizes.thumbnail.source_url;
		}
		*/

    if (!image && featuredMedia.media_details.sizes.medium) {
      image = featuredMedia.media_details.sizes.medium.source_url;
    }
    if (!image) image = featuredMedia.source_url;
    if (!image) {
      image = featuredMedia.media_details.sizes.full.source_url;
    }

    return <Image styleName="large-wide" style={{ height: 160 }} source={{ uri: image }} />;
  };

  render() {
    const { item, isBerita, ...props } = this.props;
    let time = isBerita ? moment(item.date).fromNow() : moment(item.date).format("LLL");

    return (
      <TouchableOpacity {...props}>
        <Card style={{ marginTop: 8, width: "100%" }}>
          {this.renderThumbnail()}
          <View styleName="content">
            <Subtitle>{entities.decode(item.title.rendered)}</Subtitle>
            <Caption>{time}</Caption>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}
