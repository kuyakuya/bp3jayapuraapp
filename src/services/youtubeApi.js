import config from "../config";
import axios from "axios";

const API_URL = config.YOUTUBE_API_URL;

const withQuery = (url, params = {}) => {
  const esc = encodeURIComponent;
  let query = Object.keys(params)
    .filter(key => params[key] !== "" && params[key] !== null)
    .map(key => `${esc(key)}=${esc(params[key])}`)
    .join("&");
  query = query.length > 0 ? `?${query}` : null;

  return `${url}${query}`;
};

export const getVideos = (params = {}) => {
  const defParams = {
    order: "date",
    part: "snippet",
    key: config.YOUTUBE_API_KEY,
    channelId: config.YOUTUBE_CHANNEL_ID,
    maxResults: 50
  };
  const url = withQuery(`${API_URL}/search`, { ...defParams, ...params });
  console.log(url);

  return axios.get(url);
};
